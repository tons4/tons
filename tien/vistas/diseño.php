  <?php
function cabesera( $CategoriasAlt){

    if(!empty($_SESSION['ID'])){
        echo 'Tu cuenta esta corriendo';

    }
    else{
        echo 'Tu session esta muerta';
    }
	$ht ='
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="BE Healthy Shop- e-commerce">
    <meta name="author" content="We are healthy we save de world">
    <meta name="keywords" content="">

    <title>
        BE Healthy Shop
    </title>

    <meta name="keywords" content="">

   

    <!-- styles -->
        <script src="'.Ruta.'js/jquery-1.11.0.min.js"></script>
    <link href="'.Ruta.'css/font-awesome.css" rel="stylesheet">
    <link href="'.Ruta.'css/bootstrap.min.css" rel="stylesheet">
    <link href="'.Ruta.'css/owl.carousel.css" rel="stylesheet">
    <link href="'.Ruta.'css/owl.theme.css" rel="stylesheet">
    <link href="'.Ruta.'css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="'.Ruta.'css/custom.css" rel="stylesheet">

    <script src="'.Ruta.'js/respond.min.js"></script>

    <link rel="shortcut icon" href="'.Ruta.'favicon.png">



</head>

<body>
<div id="Res"></div>
    <!-- *** inicio del modal login***
 _________________________________________________________ -->
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
            <div class="modal-dialog">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Login</h4>
                    </div>
                    <div class="modal-body">
                        <form action="customer-orders.html" method="post" id="Login2">
                            <div class="form-group">
                                <input type="text" class="form-control" id="usuario" placeholder="Usuario">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="contraseña" placeholder="Contraseña">
                            </div>

                            <p class="text-center">
                                 <p><a  class="btn btn-success" onclick="log()" >Enviar información</a></p>

                            </p>

                        </form>

                        <p class="text-center text-muted">¿Aun no estas regitrado?</p>
                        <p class="text-center text-muted"><a href="#" data-toggle="modal" data-target="#registro-modal" data-dismiss="#login-modal"><strong>Registrate ahora</strong></a>! Es facil solo te toma 1 minuto hacerlo :D</p>

                    </div>
                </div>
            </div>
        </div>

    

    <!-- *** final del modal*** -->


    <!-- *** inicio del modal Registro***
 _________________________________________________________ -->
        <div class="modal fade" id="registro-modal" tabindex="-1" role="dialog" aria-labelledby="Registro" aria-hidden="true">
            <div class="modal-dialog">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Registro">Registrar nuevo usuario</h4>
                    </div>
                    <div class="modal-body">
                        <form action="../tien/usuario/Registro" method="post">

                        <div class="form-group">
                                <input type="text" class="form-control" id="usuario" placeholder="usuario">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="pass" placeholder="Contraseña">
                            </div>
                                     <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="Nombre">Nombre completo</label>
                                        <input type="text" class="form-control" id="Nombre">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="ApellidoP">Apellido Paterno</label>
                                        <input type="text" class="form-control" id="apellidoP">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="ApellidoM">Apellido Materno</label>
                                        <input type="text" class="form-control" id="apellidoM">
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="company">Numero de casa</label>
                                        <input type="text" class="form-control" id="Direccion">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="street">Calle</label>
                                        <input type="text" class="form-control" id="street">
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->

                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label for="city">Ciudad</label>
                                        <input type="text" class="form-control" id="ciudad">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label for="zip">Codigo postal</label>
                                        <input type="text" class="form-control" id="cp">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label for="state">Estado</label>
                                        <input type="text" class="form-control" id="estado">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label for="country">Pais</label>
                                        <input type="text"  class="form-control" id="pais">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phone">Telefono</label>
                                        <input type="text" class="form-control" id="telefono">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Correo electronico</label>
                                        <input type="text" class="form-control" id="correo">
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Registrar</button>

                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>

    

    <!-- *** final del modal*** -->



    <!-- *** nav bar ***
 _________________________________________________________ -->

    <div class="navbar navbar-default yamm" role="navigation" id="navbar">
        <div class="container">
            <div class="navbar-header">

                <a class="navbar-brand home" href="'.Ruta.'" data-animate-hover="bounce">
                    <img src="'.Ruta.'img/logo.png" alt="logo" class="hidden-xs">
                    <img src="'.Ruta.'img/logo_dev.png" alt="logo" class="visible-xs"><span class="sr-only">BE Healthy Shop - inicio</span>
                </a>
                <div class="navbar-buttons">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Menu</span>
                        <i class="fa fa-align-justify"></i>
                    </button>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Buscar</span>
                        <i class="fa fa-search"></i>
                    </button>
                    ';
 if(!empty($_SESSION['ID'])){
                    $ht= $ht.'<a href="'.Ruta.'usuario"  class="btn btn-default navbar-toggle"><i class="fa fa-user">
                    </i><span class="hidden-sm"></span></a>
                    <a href="'.Ruta.'carrito" class="btn btn-default navbar-toggle"><i class="fa fa-shopping-cart">
                    </i><span class="hidden-sm"></span></a>';
                }
 else{
    $ht= $ht.'<a href="#" data-toggle="modal" data-target="#login-modal" class="btn btn-default navbar-toggle"><i class="fa fa-sign-in"></i><span class="hidden-xs">Login</span></a>';
 }               
         

$ht=$ht.'
                </div>
            </div>
            <!--/.navbar-header -->';


//mover a controlador


 $Cates ="";
 $p='1';
foreach ($CategoriasAlt as $key => $value) {


if($CategoriasAlt[$key]['NombreCategoria']== $p){
  
$li = $CategoriasAlt[$key]['NombreSubCategoria'];
   $Cates =$Cates.'<li><a href="'.RutaCate.$li.'/1">'.$li.'</a></li>';
             

  
}
elseif($p == '1'){
$Nombre = $CategoriasAlt[$key]['NombreCategoria'];
$li = $CategoriasAlt[$key]['NombreSubCategoria'];
 $Cates ='<div class="col-sm-3">
              <h5>'.$Nombre.'</h5>
              <ul><li><a href="'.RutaCate.$li.'/1">'.$li.'</a></li>';
}
else{
  $Nombre = $CategoriasAlt[$key]['NombreCategoria'];
$li = $CategoriasAlt[$key]['NombreSubCategoria'];
 $Cates =$Cates.'</ul>
              </div>
              <div class="col-sm-3">
              <h5>'.$Nombre.'</h5>
              <ul><li><a href="'.RutaCate.$li.'/1">'.$li.'</a></li>';
   
}



              $p =$CategoriasAlt[$key]['NombreCategoria'];
}

            $categoria ='

            <div class="navbar-collapse collapse" id="navigation">

                <ul class="nav navbar-nav navbar-left">
                    <li class="active"><a href="'.Ruta.'">Inicio</a>
                    </li>
                    <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Categorias<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        '.$Cates.'
                                        </div>
                                    </div>
                                </div>
                               
                            </li>
                        </ul>
                    </li>
            </div>
            <!--/.nav-collapse -->';

            $log='<div class="navbar-buttons">

               ';


 if(!empty($_SESSION['ID'])){
      $log =$log.' <div class="navbar-collapse collapse right" id="basket-overview">
                    <a href="'.Ruta.'carrito" class="btn btn-primary navbar-btn"><i class="fa fa-shopping-cart">
                    </i><span class="hidden-sm"></span></a>
                </div>

                 <div class="navbar-collapse collapse right">
                    <a href="'.Ruta.'usuario"  class="btn btn-primary navbar-btn"><i class="fa fa-user">
                    </i><span class="hidden-sm"></span></a>
                </div>
                <!--/.nav-collapse -->';

    }
    else{
       $log =$log.'
                 <div class="navbar-collapse collapse right">
                    <a href="#" data-toggle="modal" data-target="#login-modal" class="btn btn-primary navbar-btn"><i class="fa fa-sign-in">
                    </i><span class="hidden-sm">Login</span></a>
                </div>
                <!--/.nav-collapse -->';
    }
           


$log =$log.'
                <div class="navbar-collapse collapse right" id="search-not-mobile">
                    <button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Buscar</span>
                        <i class="fa fa-search"></i>
                    </button>
                </div>

            </div>

            <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Buscar" id="meme">
                        <span class="input-group-btn">

      <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>

        </span>
        <diV id="dd"></div>
                    </div>
                </form>

            </div>
            <!--/.nav-collapse -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /#navbar -->

    <!--____________________________ *** NAVBAR END _____________________________________________*** -->
';
echo $ht.$categoria.$log;
}


function foder(){
  echo ' <!-- ______________inicio del footer ____________________ -->
        <div id="footer" >
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <h4>Informacion</h4>

                        <ul>
                            <li><a href="'.Ruta.'text">Sobre Nosotros</a>
                            </li>
                            <li><a href="'.Ruta.'text">Terminos y Condiciones</a>
                            </li>
                            <li><a href="'.Ruta.'faq">Preguntas mas frecuentes</a>
                            </li>
                            <li><a href="'.Ruta.'contact">Contactanos</a>
                            </li>
                        </ul>

                        <hr>

                        <h4>Seccion de Usuarios</h4>

                        <ul>
                            <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
                            </li>
                            <li><a href="register.html">Registrate</a>
                            </li>
                        </ul>

                        <hr class="hidden-md hidden-lg hidden-sm">

                    </div>
                    <!-- fin col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>Categorias</h4>
                        <ul>
                            <li><a href="'.Ruta.'category.html">Muebles</a>
                            </li>
                            <li><a href="'.Ruta.'category.html">Ropa</a>
                            </li>
                            <li><a href="'.Ruta.'category.html">Accesorios</a>
                            </li>
                            <li><a href="'.Ruta.'category.html">Hogar</a>
                            </li>
                            <li><a href="'.Ruta.'category.html">Alimentos</a>
                            </li>
                        </ul>

                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- fin col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>Donde encontrarnos</h4>

                        <p><strong>e-commerce BE Healthy Shop.</strong>
                            <br>100 Ave. insurgentes
                            <br>col. la loma
                            <br>Tepic
                            <br>Nayarit
                            <br>
                            <strong>Mexico</strong>
                        </p>

                        <a href="contact.html">Ir a pagina de contacto</a>

                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- fin col-md-3 -->


                        <h4>Siguenos en nuestras redes sociales</h4>

                        <p class="social">
                            <a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="gplus external" data-animate-hover="shake"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="email external" data-animate-hover="shake"><i class="fa fa-envelope"></i></a>
                        </p>
                         

                    </div>
                    <!-- fin col-md-3 -->

                </div>
                <!-- fin del row -->

            </div>
            <!-- fin container -->
        </div>
        <!-- /#footer -->

        <!-- Termina footer -->




        <!-- *** COPYRIGHT ***
 _________________________________________________________ -->
        <div id="copyright">
            <div class="container">
                <div class="col-md-6">
                    <p class="pull-left">© 2018 e-commerce Be Helathy Shop</p>

                </div>
            </div>
        </div>
        <!-- *** fin copyright*** -->



   <!-- </div>-->
    <!-- -------------------------CIERRE TOTAL---------------------------------- -->


    

    <!-- ___________________scripts_____________________ -->

    <script src="'.Ruta.'js/bootstrap.min.js"></script>
    <script src="'.Ruta.'js/jquery.cookie.js"></script>
    <script src="'.Ruta.'js/waypoints.min.js"></script>
    <script src="'.Ruta.'js/modernizr.js"></script>
    <script src="'.Ruta.'js/bootstrap-hover-dropdown.js"></script>
    <script src="'.Ruta.'js/owl.carousel.min.js"></script>
    <script src="'.Ruta.'js/front.js"></script>
    <script src="'.Ruta.'js/Form.js"></script>


</body>

</html>';
}






//contenido variado


function categoria($r, $nu , $num){

 
  $contC='<div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Inicio</a>
                        </li>
                        <li><a href="#">'.$r[0]['NombreCategoria'].'</a></li>
                        <li><a href="#">'.$r[0]['NombreSubCategoria'].'</a></li>
                    </ul>
                </div>

                <div class="col-md-3">
                    <!--  ________________________ *** Menu laterales***___________________________ -->

                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Categorias</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked category-menu">
                                
                                <li class="active">
                                    <a href="category.html">'.$r[0]['NombreCategoria'].'<span class="badge pull-right">5</span></a>
                                    <ul>
                                        <li><a href="category.html">'.$r[0]['NombreSubCategoria'].'</a>
                                        </li>
                                        <li><a href="category.html">Ropa</a>
                                        </li>
                                        <li><a href="category.html">Accesorios</a>
                                        </li>
                                        <li><a href="category.html">Hogar</a>
                                        </li>
                                         <li><a href="category.html">Alimentos</a>
                                        </li>
                                    </ul>
                                </li>
                            
                            </ul>

                        </div>
                    </div>
                    

                    <!-- ----------------------Banner superior------------------- -->

                    <div class="banner">
                        <a href="#">
                            <img src="'.Ruta.'img/UTN.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>

                <div class="col-md-9">
                    <div class="box">
                        <h1>'.$r[0]['NombreCategoria'].'</h1>
                        <p>'.$r[0]['Descripcion'].'</p>
                    </div>

                    <div class="box info-bar">
                        <div class="row">
                            <div class="col-sm-12 col-md-4 products-showing">
                                Se muestran <strong>'.($num*6).'</strong> De <strong>'.$nu[0][0].'</strong> Productos
                            </div>

                        </div>
                    </div>
                    <div class="row products">
                    <!-- ------------------------------fin del banner superior--------------------- -->';



$contC2="";
foreach ($r as $key => $value) {
   $contC2='
                    <!-- --------------------inicio de lista de productos-------------------------- -->
                      

                        <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="detail.html">
                                                <img src="'.Ruta.$r[$key]['Portada'].'" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="detail.html">
                                                <img src="'.Ruta.$r[$key]['Img'].'" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="detail.html" class="invisible">
                                    <img src="'.Ruta.$r[$key]['Img'].'" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="detail.html">'.$r[$key]['Nombre'].'</a></h3>
                                    <p class="price">$'.$r[$key]['Precio'].'</p>
                                    <p class="buttons">
                                        <a href="'.Ruta.'producto/sub/'.$r[$key]['idProducto'].'" class="btn btn-default">Detalles</a>
                                        <a onclick="Regmod('.$r[$key]['idProducto'].')" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>Agregar</a>
                                    </p>
                                </div>
                                <!-- /.text -->
                            </div>
                            <!-- /.product -->
                        </div>
                      
                        
                    <!-- ---------------fin lista de productos------------ -->'.$contC2;
}




$contC=$contC.$contC2.'
                   </div>
                    <!-- /.products -->
                   <!-- --------------------------inicio paginador---------------------- -->
                    <div class="pages">';
 $ul = ($num-1);

$liF="";

 $numLi=($nu[0][0]/6);
 echo  $numLi;
if($numLi <2){
    $numLi = ($nu[0][0]/6);
    }
    else{
        $numLi = ($nu[0][0]/6)-1;
    }


for($i=0; $i<=$numLi; $i++){
    if($i == $ul){
        $liF = $liF.'<li class="active"><a href="'.($i+1).'">'.($i+1).'</a></li>';
    }
      else{
      $liF = $liF.'<li><a href="'.($i+1).'">'.($i+1).'</a></li>';
      }  
  
}
$lisBoton= '
                        <ul class="pagination">
                            <li><a href="#">&laquo;</a>
                            </li>
                            '.$liF.'
                            </li>
                            <li><a href="#">&raquo;</a>
                            </li>
                        </ul>';

$contC=$contC.$lisBoton.'
                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
';

echo $contC;
}



function localizacion($var){

print_r($var);
    $loc ='
     <div class="col-md-12">
                    <ul class="breadcrumb">
                        ';
                     

                 $loc1='';    
foreach ($var['url'] as $key => $value) {
     $loc1 = $loc1.'
                        <li><a href="'.$var['url'][$key].'">'.$var['Nombre'][$key].'</a></li>';
}
                       
                     

                        $loc =$loc.$loc1.'
                    </ul>
                </div>
';

echo $loc;
}







function CategoriaIzquierda(){
    $iz='
                  <!--  ________________________ *** Menu laterales***___________________________ -->

                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Categorias</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked category-menu">
                                ';
                                
                                 $obj='       <li><a href="category.html">Muebleria</a>
                                        </li>';
                                        $iz=$iz.'
                                    </ul>
                                </li>
                            
                            </ul>

                        </div>
                    </div>';
                    
}


function DetalleProducto($var, $Comentario, $Loc){

//posible metodo de localicacion
    $obj ='

    
                
                  
        <div id="content">
            <div class="container">
            <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Inicio</a>
                        </li>
            ';
$lc="";
foreach ($Loc[0] as $key => $value) {
  

  $lc =$lc.'<li><a href="#">'.$Loc[0][$key].'</a></li>';  
}

$obj = $obj.$lc.'</ul>
                </div>

                <div class="col-md-3">
                  <!--  ________________________ *** Menu laterales***___________________________ -->

                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Categorias</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked category-menu">
                                
                                <li class="active">
                                    <a href="category.html">Categorias<span class="badge pull-right">5</span></a>
                                    <ul>
                                        <li><a href="category.html">Muebleria</a>
                                        </li>
                                        <li><a href="category.html">Ropa</a>
                                        </li>
                                        <li><a href="category.html">Accesorios</a>
                                        </li>
                                        <li><a href="category.html">Hogar</a>
                                        </li>
                                         <li><a href="category.html">Alimentos</a>
                                        </li>
                                    </ul>
                                </li>
                            
                            </ul>

                        </div>
                    </div>
                    <!-- ------------------------Seleccion de marca---------------------------- -->

                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Marcas<a class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i>Deshacer</a></h3>
                        </div>

                        <div class="panel-body">

                            <form>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Armani (10)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Versace (12)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Carlo Bruni (15)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Jack Honey (14)
                                        </label>
                                    </div>
                                </div>

                                <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Aplicar</button>

                            </form>

                        </div>
                    </div>
                    <!-- --------------------------colores------------------------------- -->

                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Colores <a class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> Deshacer</a></h3>
                        </div>

                        <div class="panel-body">

                            <form>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour white"></span> Blanco(14)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour blue"></span> Azul (10)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour green"></span> Verde (20)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour yellow"></span> Amarillo (13)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour red"></span> Rojo (10)
                                        </label>
                                    </div>
                                </div>

                                <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Aplicar</button>

                            </form>

                        </div>
                    </div>

                    <!-- ----------------terminan Menus laterales----------------------- -->

                    <!-- ----imagen de banner--- -->
                    <div class="banner">
                        <a href="#">
                            <img src="img/Product-img/077.jpg" class="img-responsive">
                        </a>
                    </div>
                </div>

                <!-- ----------------------------imagen producto principal---------------- -->

                <div class="col-md-9">

                    <div class="row" id="productMain">
                        <div class="col-sm-6">
                            <div id="mainImage">
                                <img src="'.Ruta.$var[0]['Portada'].'" alt="" class="img-responsive">
                            </div>
                            <!-- ------etiquetas------- -->
                            <div class="ribbon sale">
                                <div class="theribbon">-10%</div>
                                <div class="ribbon-background"></div>
                            </div>
                    
                            <div class="ribbon new">
                                <div class="theribbon">Nuevo</div>
                                <div class="ribbon-background"></div>
                            </div>
                            <!-- ----fin de etiquetas--- -->

                        </div>
                        <div class="col-sm-6">
                            <div class="box">
                                <h1 class="text-center">'.$var[0]['Nombre'].'</h1>
                                <p class="goToDescription"><a href="#details" class="scroll-to">Desliza hacia abajo o clic aqui para mostrar los detalles.</a>
                                </p>
                                <p class="price">$'.$var[0]['Precio'].'</p>

                                <p class="text-center buttons">
                                    <a href="#" onclick="Regmod('.$var[0]['No'].')" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Agregar</a> 
                                    <a href="basket.html" class="btn btn-default"><i class="fa fa-heart"></i> Agregar a lista de deseados</a>
                                </p>


                            </div>

                            <!-- menu imagenes de producto para mostrar -->

                            <div class="row" id="thumbs">
                                <div class="col-xs-4">
                                    <a href="'.Ruta.$var[0]['Portada'].'" class="thumb">
                                        <img src="'.Ruta.$var[0]['Portada'].'" alt="" class="img-responsive">
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="'.Ruta.$var[0]['Img'].'" class="thumb">
                                        <img src="'.Ruta.$var[0]['Img'].'" alt="" class="img-responsive">
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="'.Ruta.$var[0]['Img2'].'" class="thumb">
                                        <img src="'.Ruta.$var[0]['Img2'].'" alt="" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                            <!-- fin menu de imagenes para mostrar -->
                        </div>

                    </div>
                    <!-- ------------------Seccion de detalles del producto_---------------------------- -->
                    <div class="box" id="details">
                        <p>
                            <blockquote>
                                <p><em>'.$var[0]['Descripcion'].'</em>
                                </p>
                            </blockquote>
<!-- --------------termina seccion de detalles del producto------------- -->
   <hr>
   <br>
                            <!-- inicio de seccion de comentarios -->
                          <h1>Comentarios</h1>
';
  


$comen="";
foreach ($Comentario as $key => $value) {
   $comen =$comen.' 
                         
                    
                    <div class="row comment">
                                <div class="col-sm-3 col-md-2 text-center-xs">
                                    <p>
                                        <img src="'.Ruta.$Comentario[$key]['ImgP'].'" class="img-responsive img-circle" alt="">
                                    </p>
                                </div>
                                <div class="col-sm-9 col-md-10">
                                    <h5>'.$Comentario[$key]['Nombre'].'</h5>
                                    <p class="posted"><i class="fa fa-clock-o"></i>'.$Comentario[$key]['fecha'].'</p>
                                    <p>'.$Comentario[$key]['comentario'].'</p>
                                </div>
                            </div>';
}


                            $obj = $obj.$comen.'
                            <!-- seccion de formulario de comentarios -->
                            <div id="comment-form" >

                            <h4>Dejar un comentarios</h4>

                            <form>
                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="comment">Comentario <span class="required">*</span>
                                            </label>
                                            <textarea class="form-control" id="comentario" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 text-right">
                                        <a class="btn btn-primary" onclick="comenU('.$var[0]['No'].')"><i class="fa fa-comment-o"></i> Comentar</a>
                                    </div>
                                </div>


                            </form>

                        </div>
                        <!-- fin seccion formulario coemntarios -->

                            <!-- fin seccion de comentarios-->
                </div>

                    </div>';


  $obj =$obj.'             
                    

                    <!-- -----------------inicia seccion de productos de interes------------ -->
                    <div class="row same-height-row">
                        <div class="col-md-3 col-sm-6">
                            <div class="box same-height">
                                <h3>Productos que tal vez te interesen.</h3>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="product same-height">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="detail.html">
                                                <img src="img/Product-img/076.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="detail.html">
                                                <img src="img/Product-img/077.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="detail.html" class="invisible">
                                    <img src="img/Product-img/078.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3>Producto 1</h3>
                                    <p class="price">$000</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="product same-height">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="detail.html">
                                                <img src="img/Product-img/078.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="detail.html">
                                                <img src="img/Product-img/077.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="detail.html" class="invisible">
                                    <img src="img/Product-img/078.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3>Producto 2</h3>
                                    <p class="price">$000</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>


                        <div class="col-md-3 col-sm-6">
                            <div class="product same-height">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="detail.html">
                                                <img src="img/Product-img/076.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="detail.html">
                                                <img src="img/Product-img/077.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="detail.html" class="invisible">
                                    <img src="img/Product-img/078.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3>producto 3</h3>
                                    <p class="price">$000</p>

                                </div>
                            </div>
                            <!-- /.product -->
                        </div>

                    </div>

                        </div>

                    </div>

                </div>

                     <!-- fin productos de interes-->
                </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->';
        echo $obj;
}


//

function ContInicio(){
  echo '


    <!-- <div id="all">-->

        <div id="content">

            <div class="container">
                <div class="col-md-12">
                    <div id="main-slider">
                        <div class="item">
                            <img src="img/Product-img/nara_1-1024x652.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="img/Slider-img/img_2.jpg" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="img/Slider-img/img_1.png" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="img/Slider-img/img_2.jpg" alt="">
                        </div>
                    </div>
                    <!-- /#main-slider -->
                </div>
            </div>

            <!--  ______________cajas de informacion____________ -->
            <div id="advantages">

                <div class="container">
                    <div class="same-height-row">
                        <div class="col-sm-4">
                            <div class="box same-height clickable">
                                <div class="icon"><i class="fa fa-heart"></i>
                                </div>

                                <h3><a href="#">Amamos a nuestro planeta</a></h3>
                                <p>Nuestro planeta debe de ser resguardado por nosotros, por eso contribuye comprando productos elaborados con materia prima 100% de materiales reciclados</p>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="box same-height clickable">
                                <div class="icon"><i class="fa fa-tags"></i>
                                </div>

                                <h3><a href="#">Mejores precios</a></h3>
                                <p>__________________________________</p>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="box same-height clickable">
                                <div class="icon"><i class="fa fa-thumbs-up"></i>
                                </div>

                                <h3><a href="#">Producto 100% de calidad</a></h3>
                                <p>__________________________________</p>
                            </div>
                        </div>
                    </div>
                    <!-- row -->

                </div>
                <!--container -->

            </div>
            

            <!-- terminan cajas de informacion -->
                    

       ';
}


//ignorar prueba de tienda 0,2
function catalogo($var){
$ht='
<style>
.hh{
  position:absolute;
  background-color: yellow;
  width: 40vh;
  visibility: hidden;
  z-index:1;
   word-break: break-all;
    word-wrap: break-word;
}
';
  foreach ($var as $key => $value) {
$ht=$ht.'
#imc'.$var[$key]['id'].'{
  background-image: url("/tienda/img/'.$var[$key]['Portada'].'");
background-repeat: no-repeat;
 background-position: center; 

       background-size: 100%;
      position: relative;
    height: 25vh;




}
#imc'.$var[$key]['id'].':hover{
  background-image: url("/tienda/img/'.$var[$key]['Img'].'");
   position: relative;

}';
}

$ht=$ht.'
</style>
';


  foreach ($var as $key => $value) {
   $ht =$ht.'<div class="cate2" id="kk">
   <div id="imc'.$var[$key]['id'].'"></div>
   <p>'.$var[$key]['Descripcion'].'</p>
   <p></p>
   <div class="hh" id="hh"><div id="imc'.$var[$key]['id'].'"></div>
   <p>'.$var[$key]['Descripcion'].'</p></div>
  </div>';
  }
  
  echo $ht;
}


?>