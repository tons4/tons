<?php

function reyenar(){
	$rr ='<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="'.Ruta.'img/Slider-img/ecologi.jpg" alt="img-1" style="width: 100%; height: 100%;" >
    </div>
    <div class="item">
      <img src="'.Ruta.'img/Slider-img/pro-1.jpg" alt="img-3" style="width: 100%; height: 100%;">
    </div>
    <div class="item">
      <img src="'.Ruta.'img/Slider-img/producto_123.png" alt="img-3" style="width: 100%; height: 100%;">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="fa fa-arrow "></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>  
      <br>
    <div id="all">

        <div id="content">

            <div class="container">
                <div class="col-md-12">
                   
                </div>
            </div>

           <!--  ______________cajas de informacion____________ -->
            <div id="advantages">

                <div class="container">
                    <div class="same-height-row">
                        <div class="col-sm-4">
                            <div class="box same-height clickable">
                                <div class="icon"><i class="fa fa-heart"></i>
                                </div>

                                <h3><a href="#">Amamos a nuestro planeta</a></h3>
                                <p>Nuestro planeta debe de ser resguardado por nosotros, por eso contribuye comprando productos elaborados con materia prima 100% de materiales reciclados</p>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="box same-height clickable">
                                <div class="icon"><i class="fa fa-tags"></i>
                                </div>

                                <h3><a href="#">Mejores precios</a></h3>
                                <p>Conprometidos con tu bolsillo asi que nuestros precios son calculados para poder ofrecer un mejor ahorro, ademas de que algunos productos incluyen un pequeño pero importante descuento</p>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="box same-height clickable">
                                <div class="icon"><i class="fa fa-thumbs-up"></i>
                                </div>

                                <h3><a>Producto 100% de calidad</a></h3>
                                <p>Nuestros productos estas elaborados con altos estandares de produccion
                                <br>
                                y bajo normativas que garantizan productos de alta calisda como si se 
                                <br>
                                tratase de materia prima nueva.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- row -->

                </div>
                <!--container -->

            </div>

             <div class="container" >
                <div class="col-md-12">
                    <div class="box slideshow">
                        <h3>Porque debemos reciclar?</h3>
                        <p class="lead">De cada 100 Kg. de basura sólo el 70% se recolecta, más de 30 mil toneladas diarias llegan a barrancos, ríos y terrenos baldíos convirtiéndose en agentes contaminantes y fuentes de infección. La mayor parte de los desechos son reutilizables y reciclables, el problema está en que al mezclarlos se convierten en basura.

En promedio una familia genera mensualmente basura constituida por papel, cartón, vidrio, metal, plásticos y desechos de control sanitario. Si se aprende a separarla adecuadamente podremos controlarlos y evitar posteriores problemas.
Al separar nuestros desperdicios correctamente antes de que se conviertan en basura es posible reducir un 80% del espacio total que ésta ocupa.

Para poder iniciar con este proceso es importante entender qué es “reciclar”. Reciclar es el proceso mediante el cual los productos de desecho son nuevamente utilizados y tiene por objeto la recuperación, de forma directa o indirecta, de los componentes que contienen los residuos urbanos.

La importancia de hacerlo es que nos puede ayudar a resolver muchos de los problemas creados por la forma de vida moderna. Se pueden salvar grandes cantidades de recursos naturales no renovables cuando en los procesos de producción se utilizan materiales reciclados. Los recursos renovables, como los árboles, también pueden ser salvados. La utilización de productos reciclados disminuye el consumo de energía. Cuando se consuman menos combustibles fósiles, se generará menos CO2 y por lo tanto habrá menos lluvia ácida y se reducirá el efecto invernadero.</p>
                        <div id="get-inspired" class="owl-carousel owl-theme">
                            <div class="item">
                                <a href="#">
                                    <img src="'.Ruta.'img/Slider-img/conta/arboles.png" alt="Get inspired" class="img-responsive">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="'.Ruta.'img/Slider-img/conta/conta_aire.jpg" alt="Get inspired" class="img-responsive">
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <img src="'.Ruta.'img/Slider-img/conta/conta_ba.jpg" alt="Get inspired" class="img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- *** GET INSPIRED END *** -->';
     echo $rr;
}


function text(){
		$rr ='<div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Inicio</a>
                        </li>
                        <li>Sobre nosotros</li>
                    </ul>
                </div>

                <div class="col-md-3">
                    <!-- *** PAGES MENU ***
 _________________________________________________________ -->
                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Mas informacion</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li>
                                    <a href="'.Ruta.'text">Sobre nosotros</a>
                                </li>
                                <li>
                                    <a href="'.Ruta.'contact">Contactanos</a>
                                </li>
                                <li>
                                    <a href="'.Ruta.'faq">Preguntas frecuentes</a>
                                </li>

                            </ul>

                        </div>
                    </div>

                    <!-- *** PAGES MENU END *** -->


                    <div class="banner">
                        <a href="#">
                            <img src="'.Ruta.'img/logo.png" alt="sales 2014" class="img-responsive">
                        </a>
                    </div>
                </div>

                <div class="col-md-9">

                    <div class="box" id="text-page">
                          <h1>Sobre nosotros</h1>

                        <p class="lead">Somos una empresa...</p>

                        <p><strong>Quese dedica a</strong> la venta de produtos ecologicos <code>E-commerce</code>, <a href="faq.html">si tiene preguntas</a> aqui se las aclaramos.</p>

                        <h2>Mision</h2>

                        <ol>
                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                            <li>Aliquam tincidunt mauris eu risus.</li>
                        </ol>

                        <blockquote>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada
                                tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p>
                        </blockquote>

                        <h2>Vision</h2>

                        <ul>
                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                            <li>Aliquam tincidunt mauris eu risus.</li>
                        </ul>
                        <br>
                         <h2>Valores</h2>

                        <ul>
                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                            <li>Aliquam tincidunt mauris eu risus.</li>
                        </ul>

                        <hr>

                        <h2>Aquipo de trabajo</h2>

                        <div class="row">
                            <div class="col-md-3">
                                <p class="text-center">
                                    <img src="'.Ruta.'img/foto_1.jpg" class="img-circle img-responsive" alt="">
                                </p>
                                <p class="text-center">Programer</p>
                            </div>
                            <div class="col-md-3">
                                <p class="text-center">
                                    <img src="'.Ruta.'img/foto_1.jpg" class="img-circle img-responsive" alt="">
                                </p>
                                <p class="text-center">Designer</p>
                            </div>
                            <div class="col-md-3">
                                <p class="text-center">
                                    <img src="'.Ruta.'img/foto_1.jpg" class="img-circle img-responsive" alt="">
                                </p>
                                <p class="text-center">Development</p>
                            </div>
                            <div class="col-md-3">
                                <p class="text-center">
                                    <img src="'.Ruta.'img/foto_1.jpg" class="img-circle img-responsive" alt="">
                                </p>
                                <p class="text-center">Documenter</p>
                            </div>
                           
                        </div>

                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->';
     echo $rr;
}


function contact(){
		$rr ='<div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="'.Ruta.'">Inicio</a>
                        </li>
                        <li>Contactanos</li>
                    </ul>

                </div>

                <div class="col-md-3">
                    <!-- *** PAGES MENU ***
 _________________________________________________________ -->
                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Mas informacion</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li>
                                    <a href="'.Ruta.'text">Sobre nosotros</a>
                                </li>
                                <li>
                                    <a href="'.Ruta.'contact">Contactanos</a>
                                </li>
                                <li>
                                    <a href="'.Ruta.'faq">Preguntas frecuentes</a>
                                </li>

                            </ul>

                        </div>
                    </div>

                    <!-- *** PAGES MENU END *** -->


                    <div class="banner">
                        <a href="#">
                            <img src="'.Ruta.'img/logo.png" class="img-responsive">
                        </a>
                    </div>
                </div>

                <div class="col-md-9">


                    <div class="box" id="contact">
                        <h1>Contactanos</h1>

                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga voluptas explicabo eum modi vero enim tenetur amet eveniet! Quisquam labore ratione pariatur ullam optio deleniti magni, quaerat tenetur. Velit, iste?</p>
                        <p>mas texto....</p>

                        <hr>

                        <div class="row">
                            <div class="col-sm-4">
                                <h3><i class="fa fa-map-marker"></i> Direccion</h3>
                                    <br>10 hidalgo
                                    <br>col. centro
                                    <br>Xalisco
                                    <br>Nayarit
                                    <br>Mexico</p>
                                </p>
                            </div>
                            <!-- /.col-sm-4 -->
                            <div class="col-sm-4">
                                <h3><i class="fa fa-phone"></i> Atencion al cliente</h3>
                                <p class="text-muted">Servicio al cliente 23/7</p>
                                <p><strong>+52 1 311-111-11-11</strong>
                                </p>
                            </div>
                            <!-- /.col-sm-4 -->
                            <div class="col-sm-4">
                                <h3><i class="fa fa-envelope"></i> Correo electronico</h3>
                                <p class="text-muted">Tambien puede contactarnos por correo electronico</p>
                                <ul>
                                    <li><strong><a href="mailto:">BEHealthyshop@gmail.com</a></strong>
                                    </li>
                                    <li><strong><a href="#footer">Redes sociales</a></strong> - Disponible tambien en nuestras redes sociales</li>
                                </ul>
                            </div>
                            <!-- /.col-sm-4 -->
                        </div>
                        <!-- /.row -->

                        <hr>

                        <div id="map">

                        </div>

                        
                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
';
     echo $rr;
}

function faq(){
		$rr ='<div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="'.Ruta.'">Inicio</a>
                        </li>
                        <li>Preguntas frecuentes</li>
                    </ul>

                </div>

                <div class="col-md-3">
                    <!-- *** PAGES MENU ***
 _________________________________________________________ -->
                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Mas informacion</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li>
                                    <a href="'.Ruta.'text">Sobre nosotros</a>
                                </li>
                                <li>
                                    <a href="'.Ruta.'contact">Contactanos</a>
                                </li>
                                <li>
                                    <a href="'.Ruta.'faq">Preguntas frecuentes</a>
                                </li>

                            </ul>

                        </div>
                    </div>

                    <!-- *** PAGES MENU END *** -->


                    <div class="banner">
                        <a href="#">
                            <img src="'.Ruta.'img/Product-img/078.jpg" alt="sales 2014" class="img-responsive">
                        </a>
                    </div>
                </div>

                <div class="col-md-9">


                    <div class="box" id="contact">
                        <h1>Preguntas Frecuentes</h1>

                        <p class="lead">Tienes o has tenido problemas al realizar una compra o dudas de nuestro servicio</p>
                        <p>Adelante informate.</p>

                        <hr>

                        <div class="panel-group" id="accordion">

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">

					    <a data-toggle="collapse" data-parent="#accordion" href="#faq1">

						1. pregunta 1?

					    </a>

					</h4>
                                </div>
                                <div id="faq1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam fugit non ex quaerat, nisi id, hic sint expedita itaque sapiente sunt aliquid ullam maiores aliquam quam ipsum iure dolorum nostrum.</p>
                                        <ul>
                                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                                            <li>Aliquam tincidunt mauris eu risus.</li>
                                            <li>Vestibulum auctor dapibus neque.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel -->

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">

					    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">

						2. pregunta 2?

					    </a>

					</h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis quos provident minus labore blanditiis, rerum commodi voluptate similique deserunt odio. Quasi doloremque ut, dicta reiciendis dolor sed error sit rerum.
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel -->


                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">

					    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">

						3. pregunta 3?

					    </a>

					</h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                       Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores ipsum maiores dolorem repudiandae eveniet esse sit reiciendis quaerat facilis repellat officia, vitae voluptates inventore voluptate at quia minima, nemo rem.
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel -->

                        </div>
                        <!-- /.panel-group -->


                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

';
     echo $rr;
}


function D404(){
	$rr= ' <div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">

                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li>Page not found</li>
                    </ul>


                    <div class="row" id="error-page">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="box">

                                <p class="text-center">
                                    <img src="img/logo.png" alt="Obaju template">
                                </p>

                                <h3>We are sorry - this page is not here anymore</h3>
                                <h4 class="text-muted">Error 404 - Page not found</h4>

                                <p class="text-center">To continue please use the <strong>Search form</strong> or <strong>Menu</strong> above.</p>

                                <p class="buttons"><a href="index.html" class="btn btn-primary"><i class="fa fa-home"></i> Go to Homepage</a>
                                </p>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->';
        echo $rr;
}

?>