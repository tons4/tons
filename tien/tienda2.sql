-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 09-08-2018 a las 03:24:04
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Categoria`
--

CREATE TABLE `Categoria` (
  `idCategoria` int(11) NOT NULL,
  `NombreCategoria` varchar(200) DEFAULT NULL,
  `Descripcion` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Categoria`
--

INSERT INTO `Categoria` (`idCategoria`, `NombreCategoria`, `Descripcion`) VALUES
(1, 'Muebleria', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irur'),
(2, 'Ropa', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irur'),
(3, 'Accesorios', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irur'),
(4, 'Hogar', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irur'),
(5, 'Alimentos', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irur');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datosUser`
--

CREATE TABLE `datosUser` (
  `idDatos` int(11) NOT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `Nombre` varchar(200) DEFAULT NULL,
  `apellidoM` varchar(200) DEFAULT NULL,
  `apellidoP` varchar(200) DEFAULT NULL,
  `Direccion` varchar(200) DEFAULT NULL,
  `telefono` varchar(200) DEFAULT NULL,
  `ciudad` varchar(250) DEFAULT NULL,
  `cp` varchar(10) DEFAULT NULL,
  `estado` varchar(200) DEFAULT NULL,
  `pais` varchar(200) DEFAULT NULL,
  `correoAlt` varchar(200) DEFAULT NULL,
  `correo` varchar(200) DEFAULT NULL,
  `imgP` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `datosUser`
--

INSERT INTO `datosUser` (`idDatos`, `idUsuario`, `Nombre`, `apellidoM`, `apellidoP`, `Direccion`, `telefono`, `ciudad`, `cp`, `estado`, `pais`, `correoAlt`, `correo`, `imgP`) VALUES
(1, 1, 'juanf', 'francisco', 'mendes', 'mexico#100', '1234567890', 'mexico', '70594', 'mexico', 'mexico', '', 'calabaza2122@gmail.com', 'img/Product-img/077.jpg'),
(2, 2, 'juan2', 'francisco2', 'mendes2', 'mexico#100', '1234567890', 'mexico', '70594', 'mexico', 'mexico', '', 'hola@hotmail.com', 'img/Product-img/077.jpg'),
(3, 5, 'gdfg', 'fdgdf', 'g', 'gdfg', '1234567890', 'fdgdf', 'fdgdf', 'gdf', 'gfdgdfg', '', 'calabaza2122@gmail.com', 'img/Product-img/077.jpg'),
(4, 6, 'HFTGHGF', 'HGF', 'HGFH', 'GFHGF', '1234567890', 'HGFH', 'GFHGF', 'HGFH', 'GFHGFHGF', '', 'calabaza2122@gmail.com', 'img/Product-img/077.jpg'),
(5, 7, 'dgfdgdf', 'gfdgfd', 'gfdgdf', '23', '1234567890', 'fdgdfg', '423423', 'fdgdf', 'dfgdf', '', 'rftertreg@hotmail.com', 'img/Product-img/077.jpg'),
(6, 8, 'HÃ©ctor Bryan ', 'Reyes ', 'Carrillo', '32', '3111936907', 'Tepic', '631751', 'Nayarit', 'MÃ©xico', '', 'hbrc95@gmail.com', 'img/Product-img/077.jpg'),
(7, 9, 'Luis Angel ', 'guerra ', 'RodrÃ­guez ', '12', '3111231212', 'tepas ', '63780', 'nayarit ', 'MÃ©xico ', '', 'luisangelguerra78@gmail.com', 'img/Product-img/077.jpg'),
(8, 10, '', '', '', '', '', '', '', '', '', '', '', 'img/Product-img/077.jpg'),
(9, 11, 'jdjdjdj', 'bzbzbdb', 'jdjdjd', 'hdhshs', '1234567890', 'xbdbdbdj', '123456', 'xbzbjdzb', 'djdjdb', '', 'pokemonrojosw@gmail.com', 'img/Product-img/077.jpg'),
(10, 17, 'dfgdfg', 'fdgdf', 'gdfgdf', '43534', '1234567890', 'hgfh', '123456', 'fgdf', 'gdfg', '', 'retretrte@hotmail.com', 'img/Product-img/077.jpg'),
(11, 18, 'dfgdfg', 'fdgdf', 'gdfgdf', '43534', '1234567890', 'hgfh', '123456', 'fgdf', 'gdfg', '', 'retretrte@hotmail.com', 'img/Product-img/077.jpg'),
(12, 19, 'dfgdfg', 'fdgdf', 'gdfgdf', '43534', '1234567890', 'hgfh', '123456', 'fgdf', 'gdfg', '', 'retretrte@hotmail.com', 'img/Product-img/077.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deseos`
--

CREATE TABLE `deseos` (
  `idDeseos` int(11) NOT NULL,
  `idProd` int(11) DEFAULT NULL,
  `idUsu` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrega`
--

CREATE TABLE `entrega` (
  `idEntrega` int(11) NOT NULL,
  `idVenta` int(11) DEFAULT NULL,
  `comentario` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productoComentario`
--

CREATE TABLE `productoComentario` (
  `idComentario` int(11) NOT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `comentario` varchar(250) DEFAULT NULL,
  `fecha` varchar(250) DEFAULT NULL,
  `idProducto` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productoComentario`
--

INSERT INTO `productoComentario` (`idComentario`, `idUsuario`, `comentario`, `fecha`, `idProducto`) VALUES
(1, 1, 'Buen producto xD', '2018-10-10', 1),
(2, 1, 'Buen producto xD2', '2018-10-10', 1),
(3, 1, 'Buen producto xD3', '2018-10-10', 1),
(4, 1, 'probando', '18-07-28', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productoInf`
--

CREATE TABLE `productoInf` (
  `idProducto` int(11) NOT NULL,
  `Nombre` varchar(200) DEFAULT NULL,
  `Descripcion` varchar(250) DEFAULT NULL,
  `Precio` float DEFAULT NULL,
  `Cantidad` int(11) DEFAULT NULL,
  `Portada` varchar(200) DEFAULT NULL,
  `Img` varchar(250) DEFAULT NULL,
  `descuento` float DEFAULT NULL,
  `Img2` varchar(250) DEFAULT NULL,
  `idSubCategoria` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productoInf`
--

INSERT INTO `productoInf` (`idProducto`, `Nombre`, `Descripcion`, `Precio`, `Cantidad`, `Portada`, `Img`, `descuento`, `Img2`, `idSubCategoria`) VALUES
(1, 'producto 1', 'blanco, corto etc.', 100, -29, 'img/Product-img/mesa_1.jpg', 'img/Product-img/076.jpg', 0, 'img/Product-img/077.jpg', 1),
(2, 'producto 2', 'blanco, corto etc.', 200, 6, 'img/Product-img/077.jpg', 'img/Product-img/078.jpg', 0, 'img/Product-img/077.jpg', 1),
(3, 'producto 3', 'blanco, corto etc.', 300, 8, 'img/Product-img/076.jpg', 'img/Product-img/077.jpg', 10, 'img/Product-img/077.jpg', 1),
(4, 'producto 1', 'blanco, corto etc.', 100, 4, 'img/Product-img/mesa_1.jpg', 'img/Product-img/076.jpg', 0, 'img/Product-img/077.jpg', 1),
(5, 'producto 2', 'blanco, corto etc.', 200, 2, 'img/Product-img/077.jpg', 'img/Product-img/078.jpg', 0, 'img/Product-img/077.jpg', 1),
(6, 'producto 3', 'blanco, corto etc.', 300, -9, 'img/Product-img/076.jpg', 'img/Product-img/077.jpg', 10, 'img/Product-img/077.jpg', 1),
(7, 'producto 1', 'blanco, corto etc.', 100, 4, 'img/Product-img/mesa_1.jpg', 'img/Product-img/076.jpg', 0, 'img/Product-img/077.jpg', 1),
(8, 'producto 2', 'blanco, corto etc.', 200, 7, 'img/Product-img/077.jpg', 'img/Product-img/078.jpg', 0, 'img/Product-img/077.jpg', 1),
(9, 'producto 3', 'blanco, corto etc.', 300, 9, 'img/Product-img/076.jpg', 'img/Product-img/077.jpg', 10, 'img/Product-img/077.jpg', 1),
(10, 'Silla ejecutiva de piel ecolÃ³gica', 'Silla ejecutiva, Con bloqueo y control de inclinaciÃ³n Espresso Coated Base, Silla ejecutiva de cuero ecolÃ³gico con control de inclinaciÃ³n de bloqueo y base recubierta', 2250, 100, 'img/Product-img/Portada9.PNG', 'img/Product-img/Img9.PNG', 10, 'img/Product-img/Img29.PNG', 2),
(11, 'Silla Ejecutiva Winchester', 'Silla Ejecutiva    *De piel ecolÃ³gica * Respaldo medio     *Tapizado en malla     *Mecanismo reclinable     *Ajuste de altura para el asiento     *Descansaâˆ’brazos de PP     *Elevacion con piston neumatico     *Base de estrella de 5 puntas ', 2000, 100, 'img/Product-img/Portada10.PNG', 'img/Product-img/Img10.jpg', 10, 'img/Product-img/Img210.jpg', 2),
(12, 'Sillas De Madera De Pino Uso Rudo', 'â€¢Marca Vanely  â€¢Modelo restaurantero  â€¢Material de la base Madera  â€¢Material de la superficie de apoyo Madera  â€¢Acabado SIN BARNIZ ', 350, 100, 'img/Product-img/Portada11.png', 'img/Product-img/Img11.PNG', 10, 'img/Product-img/Img211.jpg', 2),
(13, 'silla ecolÃ³gica de CartÃ³n', ' *Montura metÃ¡lica       *Silla reciclada que estÃ¡ a favor de la ecologÃ­a y recibe el nombre Re-Ply', 250, 100, 'img/Product-img/Portada12.PNG', 'img/Product-img/Img12.jpg', 10, 'img/Product-img/Img212.jpg', 2),
(14, 'Silla Design Lievore Altherr ', '*Estructura patÃ­n en aluminio brillante o lacado con brazos integrados     *100% eco-friendly   *Italiana Arper     *', 850, 100, 'img/Product-img/Portada13.PNG', 'img/Product-img/Img13.PNG', 10, 'img/Product-img/Img213.PNG', 2),
(15, 'Silla de madera Reciclado plataforma', '*Hecho a mano    *Acabada en alto brillo       *Materiales: madera, pegamento ', 380, 100, 'img/Product-img/Portada14.jpg', 'img/Product-img/Img14.jpg', 10, 'img/Product-img/Img214.jpg', 2),
(16, 'Mesa Eco-Friendly', '*Esta mesa de comedor es nos muestra una cautivadora manera de llevar el jardÃ­n hasta nosotros, con una especie de maceta central muy bien disimulada por la madera de la que estÃ¡ hecho el mueble.          *Una mesa rÃºstica natural al 100% ideal pa', 3500, 100, 'img/Product-img/Portada15.jpg', 'img/Product-img/Img15.jpg', 10, 'img/Product-img/Img215.jpg', 1),
(17, 'Mesa Eco-Friendly', ' Esta mesa de comedor es nos muestra una cautivadora manera de llevar el jardÃ­n hasta nosotros, con una especie de maceta central muy bien disimulada por la madera de la que estÃ¡ hecho el mueble. Una mesa rÃºstica natural al 100% ideal para interio', 3500, 100, 'img/Product-img/Portada16.jpg', 'img/Product-img/Img16.jpg', 10, 'img/Product-img/Img216.jpg', 1),
(18, 'Mesa Decorativas Hogar', 'Mesa Hecha Waterford   *100% eco-Friendly    *Mesa decorativa Sala', 1600, 100, 'img/Product-img/Portada17.PNG', 'img/Product-img/Img17.jpg', 10, 'img/Product-img/Img217.jpg', 1),
(19, 'Mesa Bilbo', '*Mesa hecha con palets, patas de hierro y cristal        *Ancho de la mesa hecha con palets: 80        *Largo de la mesa hecha con palets: 120        *Alto de la mesa hecha con palets: 40', 5580, 100, 'img/Product-img/Portada18.PNG', 'img/Product-img/Img18.PNG', 10, 'img/Product-img/Img218.PNG', 1),
(20, 'Mesa Eisenheim', '*La madera usada para esta mesa hecha con palets ha sido elegida usando lÃ¡minas envejecidas naturalmente por el paso del tiempo y respetando su color original       *Ancho de la mesa hecha con palets: 80       *Largo de la mesa hecha con palets: 120', 6000, 100, 'img/Product-img/Portada19.png', 'img/Product-img/Img19.png', 10, 'img/Product-img/Img219.png', 1),
(21, 'Mesa Tipo Palet EcolÃ³gica', '*mide 60 cm por 1 me saludos la altura es de 50 cm     *madera reciclada     *100% eco-friendly', 900, 100, 'img/Product-img/Portada20.PNG', 'img/Product-img/Img20.PNG', 10, 'img/Product-img/Img220.PNG', 1),
(22, 'Mesa Tipo Palet II', '100% eco-frienly      *medidas 90*100      ', 850, 100, 'img/Product-img/Portada21.PNG', 'img/Product-img/Img21.PNG', 10, 'img/Product-img/Img221.PNG', 1),
(23, 'Mesa De Centro (llanta EcolÃ³gico)', '*Hermosa mesa de centro o taburete. Ideal para centro de sala, roof garden o balcÃ³n. Hecho de Hilo Henequen muy resistente, ademas puedes elegir el color que gustes. Estilo vintage, con llantas para moverlo fÃ¡cilmente 2 con freno para que pueda que', 1500, 100, 'img/Product-img/Portada22.PNG', 'img/Product-img/Img22.jpg', 10, 'img/Product-img/Img222.jpg', 1),
(24, 'cama ecolÃ³gica de bambÃº', '* sumamente cÃ³modas y econÃ³micas       *de bambÃº que es un material realmente duradero, barato y vistoso         *100% ecolÃ³gica para reducir el calentamiento global una cama de bambÃº es una excelente alternativa, que ademÃ¡s le darÃ¡ un toque f', 6592, 100, 'img/Product-img/Portada23.PNG', 'img/Product-img/Img23.PNG', 10, 'img/Product-img/Img223.jfif', 3),
(25, 'Cama con Palets', '*Con materiales reciclados, para de una forma econÃ³mica, fÃ¡cil y sustentable darle un cambio a la monotonÃ­a de nuestro hogar y redecorarlo', 3000, 100, 'img/Product-img/Portada24.jpg', 'img/Product-img/Img24.jpg', 10, 'img/Product-img/Img224.jpg', 3),
(26, 'Cama con Palets Madera reciclada', '*Con materiales reciclados, para de una forma econÃ³mica, fÃ¡cil y sustentable darle un cambio a la monotonÃ­a de nuestro hogar y redecorarlo', 2800, 100, 'img/Product-img/Portada25.jpg', 'img/Product-img/Img25.jpg', 10, 'img/Product-img/Img225.jpg', 3),
(27, 'Cama con Cajones ', '*Cama 100% reciclada        *Madera reutilizable con cajones con vista espectacular        *Precio accesible para el pÃºblico', 2500, 100, 'img/Product-img/Portada26.jpg', 'img/Product-img/Img26.jpg', 10, 'img/Product-img/Img226.jpg', 3),
(28, 'Cama con Palets luz integrada ', '*Eco-friendly             *Madera reutilizable              *IntegraciÃ³n de luz             *Hermosa cama para dar toque especial para tu habitaciÃ³n', 2900, 100, 'img/Product-img/Portada27.jpg', 'img/Product-img/Img27.png', 10, 'img/Product-img/Img227.jpg', 3),
(29, 'Cama Friendly  Estilo Shabby', '*100% ecolÃ³gica         *Hermosa para decorar tu casa-habitaciÃ³n      *Estilo Shabby', 3200, 100, 'img/Product-img/Portada28.jpg', 'img/Product-img/Img28.jpg', 10, 'img/Product-img/Img228.jpg', 3),
(30, 'Estanteria Estilo Cidade', '*Eco-Friendly       *Estilo Cidade         *Con vitrina 20*60               ', 2600, 100, 'img/Product-img/Portada29.jpg', 'img/Product-img/Img29.jpg', 10, 'img/Product-img/Img229.jpg', 4),
(31, 'Estantes y estanterÃ­as ecolÃ³gicas', '*EstanterÃ­as diferentes, divertidas, econÃ³micas y lo mejor de todo â€¦ ecolÃ³gicas...!              *Hecho con una escalera tijera y algunas tablas', 1500, 100, 'img/Product-img/Portada30.jpeg', 'img/Product-img/Img30.jpg', 10, 'img/Product-img/Img230.jpg', 4),
(32, 'EstanterÃ­a para lÃ¡pices de columna', '*Eco-Friendly              *Hecha con latas de atÃºn pintadas o rollos de cinta forrados                         *Una idea que te permitirÃ¡ mantener el escritorio en perfecto estado..!            *De columnas con escalones de madera              *Re', 1400, 100, 'img/Product-img/Portada31.jpeg', 'img/Product-img/Img31.jpg', 10, 'img/Product-img/Img231.jpg', 4),
(33, 'Estante para despensa en la habitaciÃ³n', '*Con muebles que aprovechan las paredes, todo en blanco, mÃ³dulos abiertos y cerrados y la ventana de que cuenta con luz natural y ventilaciÃ³n a travÃ©s de una ventana               *Eco-Friendly          *El orden en una despensa es fundamental par', 1200, 100, 'img/Product-img/Portada32.PNG', 'img/Product-img/Img32.jpg', 10, 'img/Product-img/Img232.jpg', 4),
(34, 'EstanterÃ­a armario tipo closet', '*Tipo Closet       *EcolÃ³gico       *Variedad de madera reutilizada       *Vista espectacular para un armario tipo closet es perfecto para organizar la despensa', 1450, 100, 'img/Product-img/Portada33.PNG', 'img/Product-img/Img33.jpg', 10, 'img/Product-img/Img233.jpg', 4),
(35, 'EstanterÃ­a con MDF de Italia', '*Madera MDF        *Eco-Friendly              *destaca la librerÃ­a que reviste la pared del fondo                         *Se trata de varios mÃ³dulos consecutivos de la estanterÃ­a Random, de MDF Italia                  *Con huecos de distintos for', 1750, 100, 'img/Product-img/Portada34.PNG', 'img/Product-img/Img34.jpg', 10, 'img/Product-img/Img234.jpg', 4),
(36, 'Camisa EcolÃ³gica Estampados Diferentes', '*Poleras hechas de material reciclado           *50% algodÃ³n           *Talla M y L, manga larga y corta', 150, 100, 'img/Product-img/Portada35.PNG', 'img/Product-img/Img35.PNG', 10, 'img/Product-img/Img235.PNG', 5),
(37, 'Camisa con estampado pulmones verdes', '*Eco-Friendly          *50% de algodÃ³n              *Poleras hechas de material reciclado', 120, 100, 'img/Product-img/Portada36.PNG', 'img/Product-img/Img36.PNG', 10, 'img/Product-img/Img236.png', 5),
(38, 'Camisa De Vestir Casual Cuadriculada  Moda Japonesa', '*Tipo de corte: Slimfit         *Camisa ecolÃ³gica de moda japonesa', 180, 100, 'img/Product-img/Portada37.PNG', 'img/Product-img/Img37.PNG', 10, 'img/Product-img/Img237.png', 5),
(39, 'Camisa para Mujer ', 'Camisetas ecolÃ³gicas, para  mujer 100% ecolÃ³gicos', 100, 97, 'img/Product-img/Portada38.jpg', 'img/Product-img/Img38.PNG', 10, 'img/Product-img/Img238.png', 5),
(40, 'Camiseta Boodlife Keep Wild Large p/mujer', '*Camisetas ecolÃ³gicas para  mujer 100% ecolÃ³gicos        Tallas: S, M, L', 135, 98, 'img/Product-img/Portada39.jpg', 'img/Product-img/Img39.PNG', 10, 'img/Product-img/Img239.png', 5),
(41, 'Camisa p/mujer We Will Always Be Best Friends', '*We Will Always Be Best Friends', 125, 97, 'img/Product-img/Portada40.jpg', 'img/Product-img/Img40.PNG', 10, 'img/Product-img/Img240.png', 5),
(42, 'Pantalon Jeans Azul Cielo', '*Color: Azul cielo             * Tallas: 28, 29, 30, 32, 34,38, 40, 42, 48', 160, 100, 'img/Product-img/Portada41.jpg', 'img/Product-img/Img41.jpeg', 10, 'img/Product-img/Img241.png', 6),
(43, 'Pantalon Levi\'s ', '*Color: Azul Oscuro             * Tallas: 28, 29, 30, 32, 34,38, 40, 42', 220, 100, 'img/Product-img/Portada42.jpg', 'img/Product-img/Img42.jpg', 10, 'img/Product-img/Img242.jpeg', 6),
(44, 'Pantalones ecolÃ³gicos Nike Golf Core ', 'Nike Golf Core     *Tallas: 30, 32, 33, 34, 35, 36, 38, 40, 42    *Color: Beige  *La tela de sarga elÃ¡stica ligera proporciona todo comodidad diurna y movilidad mejorada', 250, 100, 'img/Product-img/Portada43.PNG', 'img/Product-img/Img43.PNG', 10, 'img/Product-img/Img243.PNG', 6),
(45, 'Pantalones ajustados de corte slim elÃ¡stico Perry Ellis', '*Color: Azul Oscuro             * Tallas: 30, 32, 34,38        *Adopte un enfoque deportivo para la sastrerÃ­a con un refinado pantalÃ³n de vestir Perry EllisÂ® Tech             *EcolÃ³gico         *Acabado moderno', 280, 100, 'img/Product-img/Portada44.PNG', 'img/Product-img/Img44.PNG', 10, 'img/Product-img/Img244.PNG', 6),
(46, 'Perry Ellis Portfolio Slim Fit ', '*Ajuste mÃ¡s delgado brinda un aspecto mÃ¡s contemporÃ¡neo              *Pantalon EcolÃ³gico         *Tallas: 29, 30, 31, 32, 33, 34, 35, 36, 38           Color: CafÃ© oscuro', 265, 100, 'img/Product-img/Portada45.PNG', 'img/Product-img/Img45.PNG', 10, 'img/Product-img/Img245.PNG', 6),
(47, 'Eleventy Donegal Easy Fit Pull-On Pants - Gris EspaÃ±a', '*Eleventy Donegal Easy         *Color: Gris EspaÃ±a         *Tallas: 30, 31, 32, 33, 34, 35, 38', 255, 100, 'img/Product-img/Portada46.PNG', 'img/Product-img/Img46.PNG', 10, 'img/Product-img/Img246.PNG', 6),
(48, 'Pack de calcetines ecolÃ³gicos de bambÃº', '*Pack de calcetines ecolÃ³gicos de bambÃº       *Color verde con doble rayas negras', 120, 100, 'img/Product-img/Portada47.jpg', 'img/Product-img/Img47.jpg', 10, 'img/Product-img/Img247.jpg', 7),
(49, 'Calcetines EcolÃ³gicos  Friday', '*Color: Amarillo         *Friday           *Fabricados con algodÃ³n y bambÃº', 100, 100, 'img/Product-img/Portada48.jpg', 'img/Product-img/Img48.jpg', 10, 'img/Product-img/Img248.jpg', 7),
(50, 'Calcetines EcolÃ³gicos Azules', '*Color: Azul con circulos morados            *Fabricados con algodÃ³n y bambÃº      *Para hombre             *talla: unitalla', 110, 100, 'img/Product-img/Portada49.jpg', 'img/Product-img/Img49.jpg', 10, 'img/Product-img/Img249.jpg', 7),
(51, 'CalcetÃ­n Pearl Izumi Elite - Blanco', 'Color: Blanco       *Los hilos de transferencia ELITE proporcionan una transferencia de humedad excepcional para mantener los pies mÃ¡s frescos y mÃ¡s seco               *EcolÃ³gicos', 130, 100, 'img/Product-img/Portada50.jpg', 'img/Product-img/Img50.jpg', 10, 'img/Product-img/Img250.jpg', 7),
(52, 'Drymax Sport Lite Trail Running 1/4 TripulaciÃ³n Turn Down Pack de 3 pares ', '*3 pares de calcetines ecolÃ³gicos       *color: gris espaÃ±a        *El calcetÃ­n de corte redondo se sienta sobre el tobillo               *Mantenga los pies volando alegremente a lo largo del camino en este calcetÃ­n TrailmaxÂ® trail              ', 126, 100, 'img/Product-img/Portada51.jpg', 'img/Product-img/Img51.jpg', 10, 'img/Product-img/Img251.jpg', 7),
(53, 'Darn Tough Vermont Merino Lana sobre los calcetines acolchados con relleno de becerro ', '*Performance Fit evita resbalones, amontonamientos y ampollas              *EcolÃ³gicos          *Color: verde              *Calcetines para la pantorrilla', 135, 100, 'img/Product-img/Portada52.jpg', 'img/Product-img/Img52.jpg', 10, 'img/Product-img/Img252.jpg', 7),
(54, 'Gafas de sol Modelo Terral', '*Hechas de madera      *100% ecolÃ³gicos           *tipo Rayban          *Disponen de 3 modelos bautizados con nombres malagueÃ±os: Terral, Poniente y Salitre          ', 350, 100, 'img/Product-img/Portada53.jpg', 'img/Product-img/Img53.jpg', 10, 'img/Product-img/Img253.jpg', 8),
(55, 'Gafas de sol Modelo Laveta', '*Hechas de madera      *100% ecolÃ³gicos           *tipo Rayban         ', 450, 100, 'img/Product-img/Portada54.jpg', 'img/Product-img/Img54.jpg', 10, 'img/Product-img/Img254.jpg', 8),
(56, 'Gafas de sol de madera de cerezo', '*100% ecolÃ³gico            *Madera de cerezo            *Tipo: Rayban               *Estas gafas son un producto artesanal               *Hecho a mano en AndalucÃ­a        *ErgonomÃ­a. Bisagras de acero inoxidable con muelles para un ajuste Ã³ptimo ', 500, 100, 'img/Product-img/Portada55.jpg', 'img/Product-img/Img55.jpg', 10, 'img/Product-img/Img255.jpg', 8),
(57, 'Gafas Koosh', '*Gafas ecolÃ³gicas          *Eco-Friendly         *Hechos con materiales ecolÃ³gicos         *Protege tus ojos con estos hermosas Gafas para el sol & mÃ¡s ', 800, 100, 'img/Product-img/Portada56.jpg', 'img/Product-img/Img56.jpg', 10, 'img/Product-img/Img256.jpg', 8),
(58, 'Gafas de Madera natural polarizadas EcolÃ³gicas', '*Modelo: Brazilian Sunglasses            *100% EcolÃ³gicas de madera natural', 650, 100, 'img/Product-img/Portada57.jpg', 'img/Product-img/Img57.jpg', 10, 'img/Product-img/Img257.jpg', 8),
(59, 'Gafas de madera Bamboo wood  - BAMLEYS  Lente azul claro', '*Tipo: Bamboo Wood                  *Bambleys                 *Diferentes colores de lente        ', 480, 100, 'img/Product-img/Portada58.jpg', 'img/Product-img/Img58.jpg', 10, 'img/Product-img/Img258.jpg', 8),
(60, 'Sandalias EcolÃ³gicas con fibras naturales ', '*Hechas con fibras naturales       *100% ecolÃ³gicas       *Las sandalias ecolÃ³gicas son beneficiosas en varios aspectos: son frescas porque tienen una amplia ventilaciÃ³n natural               *Evita la sudoraciÃ³n', 600, 100, 'img/Product-img/Portada59.jpg', 'img/Product-img/Img59.jpg', 10, 'img/Product-img/Img259.jpg', 9),
(61, 'Sandalias Gioseppo p/mujer', '*Sandalias Tipo: Boho-Chic Gioseppo             *Sandalias para mujer de marca para este verano          *Sandalias Romanas            ', 400, 100, 'img/Product-img/Portada60.jpg', 'img/Product-img/Img60.jpg', 10, 'img/Product-img/Img260.jpg', 9),
(62, 'Gioseppo Sandalias Para Mujer', '*Sandalias Tipo: Gioseppo                *Sandalias Romanas', 350, 100, 'img/Product-img/Portada61.PNG', 'img/Product-img/Img61.jpg', 10, 'img/Product-img/Img261.jpg', 9),
(63, 'Nae Kupe PET Blue', '*Sandalias 100% veganas y ecolÃ³gicas                *Fabricadas con PET reciclado de botellas de plÃ¡stico y suela de neumÃ¡tico reciclado', 380, 99, 'img/Product-img/Portada62. Fabricadas con PET reciclado de botellas de plaacutestico y suela de neumaacutetico reciclado B01CUD2L4W', 'img/Product-img/Img62.PNG', 10, 'img/Product-img/Img262.PNG', 9),
(64, 'Sandalias ecolÃ³gicas Recicladas', '*100% ecolÃ³gico                           *Hechas con material: neumÃ¡ticos, una materia prima secundaria que vuelve a integrarse en la cadena de producciÃ³n                         *Diferentes colores              Tallas: 25, 26, 30 ,32', 480, 100, 'img/Product-img/Portada63.jpg', 'img/Product-img/Img63.jpg', 10, 'img/Product-img/Img263.jpg', 9),
(65, 'GRUNLAND Sara, Sandalia con Pulsera para Mujer', '*Color: Beige (Kaki Kaki)                   *Tallas: 37, 38,39, 40, 41 US                    *Material exterior: Cuero               *Revestimiento: Cuero                     *Material de la suela: Corcho                  *Cierre: Hebilla            ', 340, 100, 'img/Product-img/Portada64.PNG', 'img/Product-img/Img64.PNG', 10, 'img/Product-img/Img264.PNG', 9),
(66, 'Gorras EcolÃ³gicas ', '*Color: Rojo, Azul, Negro, Amarrillo, Tinto          *Talla:Unitalla', 250, 100, 'img/Product-img/Portada65.PNG', 'img/Product-img/Img65.jpg', 10, 'img/Product-img/Img265.jpg', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SubCategoria`
--

CREATE TABLE `SubCategoria` (
  `idSubCategoria` int(11) NOT NULL,
  `NombreSubCategoria` varchar(200) DEFAULT NULL,
  `idCate` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `SubCategoria`
--

INSERT INTO `SubCategoria` (`idSubCategoria`, `NombreSubCategoria`, `idCate`) VALUES
(1, 'MESAS', 1),
(2, 'SILLAS', 1),
(3, 'CAMAS', 1),
(4, 'ESTANTERIA', 1),
(5, 'CAMISAS', 2),
(6, 'PANTALONES', 2),
(7, 'CALSETINES', 2),
(8, 'GAFAS', 3),
(9, 'SANDALIAS', 3),
(10, 'GORRAS', 3),
(11, 'BRASALETES', 3),
(12, 'RELOJS', 3),
(13, 'FUNDAS', 3),
(14, 'FORMULAS LIMPIADORAS', 4),
(15, 'ESCOBAS', 4),
(16, 'TRAPEADORES', 4),
(17, 'GALLETAS', 5),
(18, 'COMIDA', 5),
(19, 'OTROS', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `usuario` varchar(250) DEFAULT NULL,
  `pass` varchar(250) DEFAULT NULL,
  `times` int(11) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `usuario`, `pass`, `times`, `tipo`) VALUES
(1, 'Pancho', '123', 23, 1),
(2, 'juan', '123', 23, 1),
(4, 'hola', '1234567', 33, 1),
(5, 'dfgfsd', 'dgdgfd', 33, 1),
(6, 'TTTTTT', '123456', 33, 1),
(7, 'prueba123', '123456f', 33, 1),
(8, 'hbrc', 'reloj2017', 33, 1),
(9, 'Guerra', 'guerrq123', 33, 1),
(11, 'prueba1234', '123456', 33, 1),
(12, 'alejandrago', 'alejandra87', 33, 1),
(13, 'alejandragom', 'alejandra50', 33, 1),
(14, 'prueba1239', '123456', 33, 1),
(15, 'retretr', 'etretre', 33, 1),
(16, 'fdgdfg', 'fdgdfg', 33, 1),
(17, 'fdgdfg4', 'fdgdfg', 33, 1),
(18, 'fdgdfg45', 'fdgdfg', 33, 1),
(19, 'fdgdfg46', 'fdgdfg', 33, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idVenta` int(11) NOT NULL,
  `PagoFin` double DEFAULT NULL,
  `FechaVenta` date DEFAULT NULL,
  `idVentaP` varchar(250) DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`idVenta`, `PagoFin`, `FechaVenta`, `idVentaP`, `idUsuario`) VALUES
(1, 500, '2018-07-28', '1-2-', 1),
(2, 500, '2018-07-28', '1-5-6-', 1),
(3, 0, '2018-07-30', '6-5-4-1-2-', 8),
(4, 0, '2018-07-30', '6-5-', 8),
(5, 0, '2018-07-30', '6-', 8),
(6, 0, '2018-07-30', '6-', 8),
(7, 0, '2018-07-30', '6-5-', 11),
(8, 0, '2018-07-30', '6-5-', 11),
(9, 0, '2018-07-30', '6-', 11),
(10, 0, '2018-07-30', '6-5-', 11),
(11, 0, '2018-07-30', '6-5-', 11),
(12, 0, '2018-07-30', '6-5-', 11),
(13, 0, '2018-07-30', '6-5-', 11),
(14, 500, '2018-07-30', '6-5-', 11),
(15, 300, '2018-07-30', '6-', 11),
(16, 100, '2018-07-30', '34-', 11),
(17, 500, '2018-07-30', '3-5-', 11),
(18, 100, '2018-07-30', '7-', 11),
(19, 300, '2018-07-31', '6-', 8),
(20, 235, '2018-08-02', '40-39-', 13),
(21, 380, '2018-08-02', '63-', 13),
(22, 1150, '2018-08-02', '5-41-39-', 1),
(23, 360, '2018-08-02', '41-39-40-', 1),
(24, 0, '2018-08-08', '', 1),
(25, 0, '2018-08-08', '', 1),
(26, 0, '2018-08-08', '', 1),
(27, 0, '2018-08-08', '', 1),
(28, 0, '2018-08-08', '', 1),
(29, 300, '2018-08-08', '6-', 1),
(30, 300, '2018-08-08', '6-', 1),
(31, 300, '2018-08-08', '6-', 1),
(32, 300, '2018-08-08', '6-', 1),
(33, 300, '2018-08-08', '6-', 1),
(34, 300, '2018-08-08', '6-', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventaParcial`
--

CREATE TABLE `ventaParcial` (
  `idVentaP` int(11) NOT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `idProducto` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `PagoFin` float DEFAULT NULL,
  `Terminado` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventaParcial`
--

INSERT INTO `ventaParcial` (`idVentaP`, `idUsuario`, `idProducto`, `cantidad`, `PagoFin`, `Terminado`) VALUES
(1, 1, 1, 3, 300, 0),
(2, 1, 2, 1, 200, 0),
(4, 3, 6, 1, 300, 1),
(5, 6, 6, 1, 300, 1),
(6, 1, 1, 2, 200, 0),
(7, 1, 5, 1, 200, 0),
(8, 1, 6, 1, 300, 0),
(9, 8, 6, 1, 300, 0),
(10, 8, 5, 1, 200, 0),
(11, 8, 4, 1, 100, 0),
(12, 8, 1, 1, 100, 0),
(13, 8, 2, 1, 200, 0),
(14, 8, 6, 1, 300, 0),
(15, 8, 5, 2, 400, 0),
(16, 8, 6, 1, 300, 0),
(17, 8, 6, 2, 600, 0),
(18, 11, 6, 1, 300, 0),
(19, 11, 5, 2, 400, 0),
(20, 11, 6, 1, 300, 0),
(21, 11, 5, 1, 200, 0),
(22, 11, 6, 1, 300, 0),
(23, 11, 6, 1, 300, 0),
(24, 11, 5, 1, 200, 0),
(25, 11, 6, 1, 300, 0),
(26, 11, 5, 1, 200, 0),
(27, 11, 6, 1, 300, 0),
(28, 11, 5, 1, 200, 0),
(29, 11, 6, 1, 300, 0),
(30, 11, 5, 1, 200, 0),
(31, 11, 6, 1, 300, 0),
(32, 11, 5, 1, 200, 0),
(33, 11, 6, 1, 300, 0),
(34, 11, 4, 1, 100, 0),
(35, 11, 3, 1, 300, 0),
(36, 11, 5, 1, 200, 0),
(37, 11, 7, 1, 100, 0),
(38, 8, 6, 1, 300, 0),
(43, 1, 5, 4, 800, 0),
(40, 13, 40, 1, 135, 0),
(41, 13, 39, 1, 100, 0),
(42, 13, 63, 1, 380, 0),
(44, 1, 41, 2, 250, 0),
(45, 1, 39, 1, 100, 0),
(46, 1, 41, 1, 125, 0),
(47, 1, 39, 1, 100, 0),
(48, 1, 40, 1, 135, 0),
(49, 1, 6, 1, 300, 0),
(50, 1, 6, 1, 300, 0),
(51, 1, 6, 1, 300, 0),
(52, 1, 6, 1, 300, 0),
(53, 1, 6, 1, 300, 0),
(54, 1, 6, 1, 300, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Categoria`
--
ALTER TABLE `Categoria`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `datosUser`
--
ALTER TABLE `datosUser`
  ADD PRIMARY KEY (`idDatos`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `deseos`
--
ALTER TABLE `deseos`
  ADD PRIMARY KEY (`idDeseos`),
  ADD KEY `idUsu` (`idUsu`),
  ADD KEY `idProd` (`idProd`);

--
-- Indices de la tabla `entrega`
--
ALTER TABLE `entrega`
  ADD PRIMARY KEY (`idEntrega`),
  ADD KEY `idVenta` (`idVenta`);

--
-- Indices de la tabla `productoComentario`
--
ALTER TABLE `productoComentario`
  ADD PRIMARY KEY (`idComentario`),
  ADD KEY `idUsuario` (`idUsuario`),
  ADD KEY `idProducto` (`idProducto`);

--
-- Indices de la tabla `productoInf`
--
ALTER TABLE `productoInf`
  ADD PRIMARY KEY (`idProducto`),
  ADD KEY `idSubCategoria` (`idSubCategoria`);

--
-- Indices de la tabla `SubCategoria`
--
ALTER TABLE `SubCategoria`
  ADD PRIMARY KEY (`idSubCategoria`),
  ADD KEY `idCate` (`idCate`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idVenta`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `ventaParcial`
--
ALTER TABLE `ventaParcial`
  ADD PRIMARY KEY (`idVentaP`),
  ADD KEY `idUsuario` (`idUsuario`),
  ADD KEY `idProducto` (`idProducto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Categoria`
--
ALTER TABLE `Categoria`
  MODIFY `idCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `datosUser`
--
ALTER TABLE `datosUser`
  MODIFY `idDatos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `deseos`
--
ALTER TABLE `deseos`
  MODIFY `idDeseos` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `entrega`
--
ALTER TABLE `entrega`
  MODIFY `idEntrega` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productoComentario`
--
ALTER TABLE `productoComentario`
  MODIFY `idComentario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `productoInf`
--
ALTER TABLE `productoInf`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT de la tabla `SubCategoria`
--
ALTER TABLE `SubCategoria`
  MODIFY `idSubCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idVenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `ventaParcial`
--
ALTER TABLE `ventaParcial`
  MODIFY `idVentaP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
