<?php
define('Ruta', '/tien/');
define('RutaCate', '/tien/categoria/sub/');




//formularios

define("usuarioEdit",  [
	"Titulo" => "Editar de usuario",
	"control" => "EditarU",
	"Net"  => [
		0 => "Su Usuario:",
		1 => "Su Contraseña:",
		2 => "Su Nombre",
		3 => "Su Apellido Materno",
		4 => "Su Apellido Paterno",
		5 => "Su Direccion",
		6 => "Su telefono",
		7 => "Su correo",
		 8 => "Su ciudad",
         9 => "Su cp",
         10 => "Su estado",
         11 => "Su pais"
		
	],
	"id" => 
	[
    0=> "usuario",
    1=> "pass",
    2 => "Nombre",
	3 => "apellidoM",
	4 => "apellidoP",
	5 => "Direccion",
	6 => "telefono",
	7 => "correo",
    8 => "ciudad",
    9 => "cp",
    10 => "estado",
    11 => "pais"

	],
	"Tipo" =>[
     
     0 =>"form",
     1 =>"form",
     2 =>"form",
     3 =>"form",
     4 =>"form",
     5 =>"form",
     6 =>"form",
     7 =>"form",
     8 =>"form",
     9 =>"form",
     10 =>"form",
     11 =>"form"
     

	]

]);

define("usuarioRegistro",  [
	"Titulo" => "Registro de usuario",
	"control" => "RegistroU",
	"Net"  => [
		0 => "Su Usuario:",
		1 => "Su Contraseña:",
		2 => "Su Nombre",
		3 => "Su Apellido Materno",
		4 => "Su Apellido Paterno",
		5 => "Su Direccion",
		6 => "Su telefono",
		7 => "Su correo",
		 8 => "Su ciudad",
         9 => "Su cp",
         10 => "Su estado",
         11 => "Su pais"
		
	],
	"id" => 
	[
    0=> "usuario",
    1=> "pass",
    2 => "Nombre",
	3 => "apellidoM",
	4 => "apellidoP",
	5 => "Direccion",
	6 => "telefono",
	7 => "correo",
    8 => "ciudad",
    9 => "cp",
    10 => "estado",
    11 => "pais"

	],
	"Tipo" =>[
     
     0 =>"form",
     1 =>"form",
     2 =>"form",
     3 =>"form",
     4 =>"form",
     5 =>"form",
     6 =>"form",
     7 =>"form",
     8 =>"form",
     9 =>"form",
     10 =>"form",
     11 =>"form"
     

	]

]);



define("categoriaRegistro",  [
	"Titulo" => "Registro de Categoria",
	"control" => "categoriaRegistro",
	"Net"  => [
		0 => "Nombre de categoria:",
		1 => "Descripcion:"
		
	],
	"id" => 
	[
    0=> "NombreCategoria",
    1=> "Descripcion"

	],
	"Tipo" =>[
     
     0 =>"form",
     1 =>"form"
     

	]

]);

define("SubcategoriaRegistro",  [
	"Titulo" => "Registro de SubCategoria",
	"control" => "SubcategoriaRegistro",
	"Net"  => [
		0 => "Nombre de Sub categoria:"
		
	],
	"id" => 
	[
    0=> "NombreSubCategoria"

	],
	"Tipo" =>[
     
     0 =>"form"
     

	]

]);


define("productoRegistrar",  [
	"Titulo" => "Nuevo producto",
	"control" => "registro",
	"Net"  => [
		0 => "Nombre del producto:",
		1 => "Descripcion :",
		2 => "Precio :",
		3 => "Cantidad",
		4 => "Sube una portada",
		5 => "Sube una imagen del producto",
		6 => "Sube una imagen del producto 2",
		7 => "Descuento ?"
		
	],
	"id" => 
	[
    0=> "Nombre",
    1=> "Descripcion",
    2=> "Precio",
    3=>"Cantidad",
    4=>"Portada",
    5=>"Img",
    6=>"Img2",
    7=>"Descuento"


	],
	"Tipo" =>[
     
     0 =>"form",
     1 =>"form",
     2 =>"form",
     3 =>"form",
     4 =>"img",
     5 =>"img",
     6 =>"img",
     7 =>"form"
     

	]

]);



//prueba

define("part1Producto",  [
	"Titulo" => "Registro de Producto parte1",
	"control" => "producto",
	"Net"  => [
		0 => "Seleciona categoria:"
		
	],
	"id" => 
	[
    0=> "idCategoria"

	],
	"Tipo" =>[
     
     0 =>"Select"
     

	]

]);

define("part2Producto",  [
	"Titulo" => "Registro de Producto parte2",
	"control" => "producto",
	"Net"  => [
		0 => "Seleciona Subcategoria:"
		
	],
	"id" => 
	[
    0=> "idSubCategoria"

	],
	"Tipo" =>[
     
     0 =>"Select"
     

	]

]);
?>