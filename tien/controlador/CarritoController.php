<?php
include '../tien/modelo/Carrito.php';
class CarritoController {
	public function index(){
		$br = new ModeloCarrito();
		$br2 = new ModeloCategoria();
		$CategoriasAlt= $br2->categorianav();
		$date = $br->CarritoBus();
		cabesera($CategoriasAlt);
		FunctionName($date);
		foder();
	}
	public function mod($var){
		if(!empty($_SESSION['ID'])){
			$arry =[
			'idProducto'=>$var['id']
			];
			$br = new ModeloCarrito();
			echo $br->mod($arry);
		}
		else
		{
			echo '<script src="'.Ruta.'js/sweetalert.min.js"></script><script>swal("ERROR!", "Debes iniciar session para poder agregar productos al carrito :(", "warning");</script>';
		}
	}
	public function modDel($var){
		$arry =[
		'idProducto'=>$var['id']
		];
		$br = new ModeloCarrito();
		$br->modDel($arry);
		$date = $br->CarritoBus();
		FunctionName($date);
	}
	public function modedit($var){
		$arry =[
		'id'=>$var['id'],
		'canti'=>$var['canti'],
		'idPro'=>$var['idele']
		];
		$br = new ModeloCarrito();
		if($arry['canti']>0){
			$br->modedit($arry);
			$date = $br->CarritoBus();
			FunctionName($date);
		}
		else{
			$date = $br->CarritoBus();
			FunctionName($date);
		}
	}
	public function modfin($var){
		$obj = explode('-', $var['id']);
		$can = count ($obj);
		$br = new ModeloCarrito();
		$no=$br->dateCorreo($can ,$obj);
		$al="";
		for ($i=0; $i < ($can-1) ; $i++) { 
			//print_r($no);
			$al=$al.'<p> * '.$no[$i]['Nombre'].' Cantidad: '.$no[$i]['Cantidad'].' Por este productos pagas: '.$no[$i]['PagoFin'].' </p>';
		}
		$br->finmod($obj, $can, $var['id']);
		$br = new ModeloUsuario();
		$JJ = $br->EditVer();
		$br = new ModeloProducto();
		$objs = $br->ventaFinall();
		$obj= explode('-', $objs[0]['idProductos']);
		$can = count ($obj);
		//print_r($obj);
		$arr =[
		'correo'=>$JJ[0][7],
		'Nombre'=>$JJ[0][2],
		'Direccion'=>$JJ[0][5],
		'CP'=>$JJ[0][9]
		];
		$Total =[
		'Producto'=>$al,
		'Precio'=>$objs[0]['PagoFin']
		];
		$mensaje=" ".$Total['Producto'].' <tr><td>  Con un total a pagar: '.$Total['Precio'].'</tr></td>';
		$br = new ModeloCarrito();
		echo $br->correo($arr, $mensaje);
	}
}
?>