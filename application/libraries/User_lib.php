<?php
class User_lib
{
	var $CI;
  	function __construct()
	{
		$this->CI =& get_instance();
	}
	//Alain Multiple Payments
	
	//Obtiene el usuario 
	function get_user()
	{
		if(!$this->CI->session->userdata('datos_user'))
			$this->set_user(-1);
		return $this->CI->session->userdata('datos_user');
	}
	//Guarda el usuario
	function set_user($datos_user)
	{
		$this->CI->session->set_userdata('datos_user',$datos_user);
	}
	//
	function remove_user()
	{
		$this->CI->session->unset_userdata('datos_user');
	}
	//
	function clear_all()
	{
		$this->remove_user();
		
	}



	function set_carrito($nuevo_Producto)
	{
		$datos_Pasados =$this->get_carrito();
        $agregado=1;
		if(!$datos_Pasados)
		{
			$this->set_carrito_inicio($nuevo_Producto);
		}
		else
		{

            $verificar=$this->no_repetir($datos_Pasados, $nuevo_Producto); 
            if($verificar['verificado'] == 1)
            {
        	   array_push($datos_Pasados, $nuevo_Producto);
		       $this->CI->session->set_userdata('datos_carrito', $datos_Pasados);
            }
            else
            {
             $agregado=0;
            }	
		}

		return $agregado;
		
	}

	function no_repetir($datos_Pasados, $datos_nuevos)
	{
		$verificado['verificado'] = 1;
		foreach ($datos_Pasados as $key) 
		{
           
			if($key['idProducto'] == $datos_nuevos['idProducto'])
			{
				
				$verificado['verificado']=0;
				break 1;
			}
		}
		return $verificado;

	}



	function set_carrito_inicio($nuevo_Producto)
	{
		$datos =[$nuevo_Producto];
		$this->CI->session->set_userdata('datos_carrito', $datos);
	}

	function set_carrito_actualizar($nuevo_cambios)
	{
	   $this->CI->session->set_userdata('datos_carrito', $nuevo_cambios);	
	}

	function set_editar_cantidad($cantidad, $id)
	{
		$datos_Pasados =$this->get_carrito();
		foreach ($datos_Pasados as $key => $value) 
		{
			if($datos_Pasados[$key]['idProducto']== $id)
			{
				$datos_Pasados[$key]['cantidad']=$cantidad;
				$datos_Pasados[$key]['precioFinal']=($cantidad*$datos_Pasados[$key]['precioUnitario']);
			}
		}
		$this->set_carrito_actualizar($datos_Pasados);
	}


	function get_carrito()
	{
		
		return $this->CI->session->userdata('datos_carrito');
	}

	function set_eliminar_carrito($idCarrito)
	{
		$datos_Pasados =$this->get_carrito();
		foreach ($datos_Pasados as $key => $value) 
		{
			if($key == $idCarrito)
			{
				unset($datos_Pasados[$key]);
			}

		}
		$this->set_carrito_actualizar($datos_Pasados);

	}

	function remove_carrito()
	{
		$this->CI->session->unset_userdata('datos_carrito');
	}



}
?>