<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Productos extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Producto');
	}

	function index($Subcategoria='MESAS' ,$pagina_Actual=1)
	{
		$data['categorias_nav']=navegacion_categorias($this->Categoria->categorias());
		$Subcategoria=urldecode($Subcategoria);
        $dato_Subcategoria = $this->Subcategoria->buscar_nombre_subcategoria($Subcategoria);
        if($dato_Subcategoria->num_rows()>0)
        {
		   
		    $data['datos_categoria']=$dato_Subcategoria->row();
		    $data['vista']='producto';
	        $data['data']= 
		    [
	   	       'tipo'=>'catalogo',
	   	       'productos'=>ver_productos($this->Producto->ver_productos(3,$pagina_Actual, $Subcategoria),$pagina_Actual, $Subcategoria)
	   	    ];   


		    $this->load->view('master_page',$data);
	   }
	   else
	   {    $data['vista']='404';   
	   	    $this->load->view('master_page',$data);

	   }
	}

	function busca_producto()
	{
		$var = $this->input->post('Nombre');
		if($var != null)
		{

			$datos['respuesta']=buscar_producto($this->Producto->buscar_producto($var));
			$this->load->view('respuesta', $datos);
		}
		
	}

	function detalles($idProducto=1)
	{
		$datos_Producto=$this->Producto->buscar_producto_id($idProducto);
		$this->load->model('Comentario');
		$data['categorias_nav']=navegacion_categorias($this->Categoria->categorias());
		$data['vista']='producto';
		$data['data']= 
		[
			'producto'=>$datos_Producto->row(),
			'comentarios'=>comentarios_producto($idProducto),
			'tipo'=>'detalles'
		];

		$this->load->view('master_page',$data);
	}

	function registrar_producto()
	{
       
	}
}
?>