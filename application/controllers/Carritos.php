<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Carritos extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Carrito');
		$this->load->model('Producto');
		$this->load->model('Venta');
		$this->load->helper('carrito');
	}

	public function index()
	{
		$productos = $this->user_lib->get_carrito();
		$respuesta= elementos_carrito($productos);
		
		$data['vista']='carrito';
		$data['data']=
		[
			'js'=>$respuesta['js'],
			'idProductos'=>$respuesta['idProductos'],
			'contenido'=>$respuesta['contenido'],
			'cantidad'=>count($productos),
			'total'=>$respuesta['total']

		];
         $data['categorias_nav']=navegacion_categorias($this->Categoria->categorias());
		$this->load->view('master_page', $data);


	}

	public function subir()
	{
		$idUsuario = $this->user_lib->get_user();
		//if($idUsuario != -1)
		//{

		    $var = $this->input->post();
		    $producto=$this->Producto->buscar_producto_id($var['id'])->row();
		    /*$datos_Carrito =
		    [
			    'idProducto'=>$produto->idProducto,
			    'idUsuario'=> $idUsuario,
			    'cantidad'=>1,
			    'PagoFin'=>$produto->Precio,
			    'Terminado'=>1
		     ];*/
             $datos_Carrito =
		     [
		     	
		     		'idProducto'=>$producto->idProducto,
			        'cantidad'=>1,
			        'nombre'=>$producto->Nombre,
			        'precioUnitario'=>$producto->Precio,
			        'precioFinal'=>($producto->Precio*1),
			        'img'=>$producto->Portada,
			        'url'=>base_url().'detalles/'.$producto->idProducto
		     	
			
		    ];
		    $respuesta = $this->user_lib->set_carrito($datos_Carrito);
		    /*$respuesta =$this->Carrito->selecionar_producto($datos_Carrito);*/
       
		    if($respuesta !=0)
		    {

			    $data['tipo'] ="success";
			    $data['mensaje'] = "Tu producto a sido agregado al carrito";
			    $data['titulo'] = "Excelente";
			    $data['recargar'] = false;
		    }
		    else
            {
        	    $data['tipo'] ="warning";
			    $data['mensaje'] = "Vaya parese que ya haz agregado este producto";
			    $data['titulo'] = "ERROR!";
			    $data['recargar'] = false;
            }
        //}

        /*else
        {
        	$data['tipo'] ="warning";
			$data['mensaje'] = "inicia sesion para usar esta funcion";
			$data['titulo'] = "ERROR!";
			$data['recargar'] = false;
        }*/ 

        $this->load->view('respuesta',$data);	


	}

	public function editar()
	{
		$var = $this->input->post();
		$this->user_lib->set_editar_cantidad($var['cantidad'], $var['idProducto']);
		$productos = $this->user_lib->get_carrito();
		$respuesta= elementos_carrito($productos);
		
		$data=
		[
			'js'=>$respuesta['js'],
			'idProductos'=>$respuesta['idProductos'],
			'contenido'=>$respuesta['contenido'],
			'cantidad'=>count($productos),
			'total'=>$respuesta['total']

		];
		$this->load->view('carrito', $data);

	}

	public function eliminar()
	{
		$var = $this->input->post();
		$this->user_lib->set_eliminar_carrito($var['id']);
		$productos = $this->user_lib->get_carrito();
		$respuesta = elementos_carrito($productos);
		
		$data=
		[
			'js'=>$respuesta['js'],
			'idProductos'=>$respuesta['idProductos'],
			'contenido'=>$respuesta['contenido'],
			'cantidad'=>count($productos),
			'total'=>$respuesta['total']

		];
		$this->load->view('carrito', $data);
	}

	public function comprar()
	{
		$datos_usuario = $this->user_lib->get_user();
	    if($datos_usuario != -1)
	    {
	        $datos_Carrito = $this->user_lib->get_carrito();
	        if(count($datos_Carrito)>0)
	        {

	    	   $config =
		       [
               'protocol' => 'smtp',
               'stmp_crypto'=> 'ssl',
               'smtp_host' => 'ssl://smtp.googlemail.com',
               'smtp_port' => 465,
               'smtp_user' => 'behealthyshop87@gmail.com',
               'smtp_pass' => 'wwyokupsbktyvkrb',
               'mailtype'  => 'html', 
               'charset'   => 'iso-8859-1',
               'newline'  => "\r\n"
    
               ];

           
           
           
                $filename = APPPATH.'/img/logo.png';
                $this->load->library('email', $config);
                $this->email->from('behealthyshop87@gmail.com', 'bien realizaste una compra '.$datos_usuario['usuario']);
                $this->email->to($datos_usuario['correo']);
                $this->email->subject('datos de la compra');
                $this->email->attach($filename);
                $cid = $this->email->attachment_cid($filename);
                $data = compra($datos_Carrito, $cid);
                $this->email->message($data['datos']);

                if($this->email->send())
                {
                	 $data['tipo'] ="success";
	                 $data['mensaje'] = "la compra fue exitosa";
	                 $data['titulo'] = "Excelente";
	                 $data['recargar'] = true;
	                 $datos_insertar=
	                 [
	                 	'PagoFin'=>$data['total'],
	                 	'FechaVenta'=>date("y-m-d"),
	                 	'idVentaP'=>$data['idProductos'],
	                 	'idUsuario'=>$datos_usuario['id']
	                 ];
	                 $this->Venta->insertar_venta($datos_insertar);


	                 $this->user_lib->remove_carrito();
                }
                else
                {
                	 $data['tipo'] ="warning";
	                 $data['mensaje'] = "la compra no se pudo efectuar";
	                 $data['titulo'] = "Error";
	                 $data['recargar'] = false;
                }
            }
            else
            {
            	$data['tipo'] ="warning";
		        $data['mensaje'] = "no hay productos en el carrito";
		        $data['titulo'] = "ERROR!";
		        $data['recargar'] = true;
            }	




	    }
	    else
        {
    	    $data['tipo'] ="warning";
		    $data['mensaje'] = "inicia sesion para usar esta funcion";
		    $data['titulo'] = "ERROR!";
		    $data['recargar'] = true;
        } 

    $this->load->view('respuesta',$data);	  		
    }

}
?>