<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {
	
	function __construct() {
		
		parent::__construct();
        
	}

	public function index()
	{
        
		/* $datos=['cate'=>''];*/
		//$this->Categoria->categorianav();
		//$this->Venta->venta()->result();
		$data["categorias_nav"]=navegacion_categorias($this->Categoria->categorias());
		$this->output->enable_profiler(TRUE);     
		$this->load->view('front',$data);
		
		
	}

	public function error()
	{
		$data["categorias_nav"]=navegacion_categorias($this->Categoria->categorias());
		$data['vista']= '404';
		$this->load->view('master_page', $data);

	}


}
