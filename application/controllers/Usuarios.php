<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuario');
	}


	public function index()
	{

        
	}

	public function login()
	{
		$var= $this->input->post();
		$verificar = $this->Usuario->login($var['usuario'], $var['Pass']);
		if($verificar->num_rows() >0)
		{
			$data['tipo'] ="success";
			$data['mensaje'] = "Sesion iniciada";
			$data['titulo'] = "Excelente";
			$data['recargar'] = true;

			$datos=$verificar->row();
			$datos_user =
			[
				'id'=>$datos->idUsuario,
				'usuario' =>$datos->usuario,
				'tipo'=>$datos->tipo,
				'correo'=>$datos->correo
			];
			
			$this->user_lib->set_user($datos_user);
			

		}

		else
		{
			$data['tipo'] ="warning";
			$data['mensaje'] = "Tu contraseña o usuario es incorrecto ";
			$data['titulo'] = "ERROR!";
			$data['recargar'] = false;
		}

		$this->load->view('respuesta',$data);

	}

	public function salir()
	{
		$this->user_lib->clear_all();
		header('Location: '.base_url());
	}

	public function registrar()
	{
		$var= $this->input->post();

   	    $datos_Usuario =
   	    [

		     'datosUsuario'=>['usuario'=>$var['usuario'],
		       'pass'=>$var['pass'],
		       'times'=>23,
		       'tipo'=>1],
		       
		     'datosPersonales'=>['idUsuario'=>'',
		     	'nombre'=>$var['Nombre'],
		        'apellidoP'=>$var['apellidoP'],
		        'apellidoM'=>$var['apellidoM'],
		        'Direccion'=>$var['Direccion'],
		        'telefono'=>$var['telefono'],
		        'ciudad'=>$var['ciudad'],
		        'cp'=>$var['cp'],
		        'estado'=>$var['estado'],
		        'pais'=>$var['pais'],
		        'correo'=>$var['correo'],
		        'imgP'=> 'img/Product-img/077.jpg']              
	    ];
		
		$var =$this->Usuario->Registrar($datos_Usuario);
        
        if($var != false)
        {
        	$data['tipo'] ="success";
			$data['mensaje'] = "se guardaron los datos";
			$data['titulo'] = "Excelente";
			$data['recargar'] = true;

        }
        else
        {
        	$data['tipo'] ="warning";
			$data['mensaje'] = "tu correo o usuario ya estan en uso, seleciona otro";
			$data['titulo'] = "ERROR!";
			$data['recargar'] = false;
        }	

        $this->load->view('respuesta',$data);

	}
}
?>