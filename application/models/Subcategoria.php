<?php
class Subcategoria extends CI_model
{
	
	
	function subcategorias()
	{
		$this->db->from('subcategoria');
		$this->db->order_by('idSubCategoria', 'asc');
		return $this->db->get();
	}

	function buscar_subcategoria($idSubcategoria=1)
	{
		$this->db->from('subcategoria');
		$this->db->where('idCate',$idSubcategoria);
		return $this->db->get();
	}

	function buscar_nombre_subcategoria($Nombre='MESAS')
	{
		$this->db->from('subcategoria');
		$this->db->join('Categoria','Categoria.idCategoria = SubCategoria.idCate');
		$this->db->where('NombreSubCategoria',$Nombre);
		return $this->db->get();
	}
}

?>