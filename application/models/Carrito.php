<?php
class Carrito extends CI_Model
{
	
  
	function carritos()
	{
		$this->db->from('ventaparcial');
		$this->db->order_by('idVentaP','ASC');
		return $this->db->get();
	}

	function buscar_carrito($IdUsuario=1)
	{
		$this->db->from('ventaparcial');
		$this->db->join('productoInf', 'productoInf.idProducto = ventaparcial.idProducto');
		$this->db->where('ventaparcial.idUsuario',$IdUsuario);
		$this->db->where('ventaparcial.Terminado !=',0);
		return $this->db->get();
	}

	function no_repetir($var)
	{
		$this->db->from('ventaparcial');
		$this->db->where('idUsuario',$var['idUsuario']);
		$this->db->where('idProducto',$var['idProducto']);
		$this->db->where('Terminado',1);
		return $this->db->get()->num_rows();


	}

	function selecionar_producto($var)
	{
		$ver=$this->no_Repetir($var);
		if($ver ==0)
		{
			$this->db->insert('ventaparcial', $var);
		    return $this->db->affected_rows();
		}
		else 
		{
			return false;
		}	
		
        
	}
}

?>