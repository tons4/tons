<?php
class Venta extends CI_model
{
	
	
	function ventas ()
	{
		$this->db->from('venta');
		$this->db->order_by('idVenta','asc');
		return $this->db->get();
	}

	function buscar_venta($idUsuario=1)
	{
		$this->db->from('venta');
		$this->where('idUsuario',$idUsuario);
		return $this->db->get();
	}

	function insertar_venta($datos)
	{
		$this->db->insert('venta',$datos);
	}
}
?>