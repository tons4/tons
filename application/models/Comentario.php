<?php
class Comentario extends CI_model
{
	
	
	function comentarios()
	{
		$this->db->from('productocomentario');
		$this->db->order_by('fecha', 'desc');
		return $this->db->get();
	}

	function buscar_comentario($idProducto=1)
	{
		$this->db->from('productocomentario');
		$this->db->join('datosUser','datosuser.idUsuario=productocomentario.idUsuario');
		$this->db->order_by('fecha', 'desc');
		$this->db->where('idProducto', $idProducto);
		return $this->db->get();
	}
}
?>