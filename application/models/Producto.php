<?php
class Producto extends CI_model
{
	
	
	function productos()
	{
		$this->db->from('productoInf');
		$this->db->order_by('Nombre', 'ASC');
		return $this->db->get();
	}

	function buscar_producto($Nombre='producto')
	{
		$this->db->from('productoInf');
		$this->db->order_by('Nombre', 'asc');
		$this->db->like('Nombre',$Nombre);
		return $this->db->get();
	}

	function buscar_producto_id($id=1)
	{
		$this->db->from('productoInf');
		$this->db->where('idProducto',$id);
		return $this->db->get();
	}

	function ver_cantidad_Producto($NombreCategoria='MESAS')
	{
		$this->db->select('count(*) as Canti');
    	$this->db->from('productoInf');
    	$this->db->join('SubCategoria','SubCategoria.idSubCategoria = productoInf.idSubCategoria');
    	$this->db->join('Categoria','Categoria.idCategoria = SubCategoria.idCate');
    	$this->db->where('SubCategoria.NombreSubCategoria', $NombreCategoria);
    	return $this->db->get();
	}
 
    function ver_productos($limit=6, $offset=0, $NombreCategoria)
    {
    	if($offset<0){$offset=1;}
    	
    	$this->db->from('productoInf');
    	$this->db->join('SubCategoria','SubCategoria.idSubCategoria = productoInf.idSubCategoria');
    	$this->db->join('Categoria','Categoria.idCategoria = SubCategoria.idCate');
    	$this->db->where('SubCategoria.NombreSubCategoria', $NombreCategoria);
    	$this->db->order_by("productoInf.idProducto", "asc");
    	$this->db->limit($limit);
		$this->db->offset($offset);
    	return $this->db->get();

    }
    

}


?>