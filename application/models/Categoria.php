<?php
class Categoria extends CI_Model 
{

	//
	function categorias()
	{
		$this->db->select('idCategoria, NombreCategoria');
		$this->db->from('categoria');	
		$this->db->order_by("NombreCategoria", "asc");
		return $this->db->get();
	}
	//
	function exists($idCategoria)
	{
		$this->db->from('categoria');
		$this->db->where('idCategoria',$idCategoria);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	//
	function registrar(&$data, $idCategoria=false)
	{
		if (!$idCategoria or !$this->exists($idCategoria))
		{
			if($this->db->insert('categoria',$data))
			{
				$data['idCategoria']=$this->db->insert_id();
				return true;
			}
			else
			{
			return false;
			}
		}
		else
		{
		$this->db->where('idCategoria', $idCategoria);
		$data['idCategoria']=$idCategoria;
		return $this->db->update('categoria',$data);
		}		
	}
	//
	function categoria_nav($limit=10000, $offset=0)
	{
		$this->db->from('Categoria');
		$this->db->join('SubCategoria','Categoria.idCategoria=SubCategoria.idCate');
		$this->db->order_by("Categoria.idCategoria", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
}
?>