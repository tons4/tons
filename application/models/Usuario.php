<?php
class Usuario extends CI_model
{
	
	
	function usuarios()
	{
		$this->db->from('usuarios');
		$this->db->order_by('idUsuario', 'desc');
		return $this->db->get();
	}

	function login($usuario, $pass)
	{
		$this->db->from('usuarios');
		$this->db->join('datosuser','datosuser.idUsuario=usuarios.idUsuario');
		$this->db->where('usuario',$usuario);
		$this->db->where('pass',$pass);
		return $this->db->get();
	}

	function recuperar_datos($idUsuario=1)
	{
		$this->db->from('usuarios');
		$this->db->join('datosuser','datosuser.idUsuario=usuarios.idUsuario');
		$this->db->where('idUsuario', $idUsuario);
		return $this->db->get();
	}

	function verificar($usuario, $email)
	{
		$this->db->from('usuarios');
		$this->db->join('datosuser','datosuser.idUsuario=usuarios.idUsuario');
		$this->db->where('usuario',$usuario);
		$this->db->where('correo',$email);
		return $this->db->get()->num_rows();
	}

    

	function Registrar($valores)
	{

		$varificado = $this->verificar($valores['datosUsuario']['usuario'], $valores['datosPersonales']['correo']);
		if($varificado ==0)
		{
			$this->db->insert('usuarios', $valores['datosUsuario']);
			$valores['datosPersonales']['idUsuario']=$this->db->insert_id();
			$this->db->insert('datosuser', $valores['datosPersonales']);
			return $this->db->affected_rows();

		}
		else
		{
			return  false;
		}	


	}


}
?>