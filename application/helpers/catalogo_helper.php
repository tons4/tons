<?php
//
function navegacion_categorias($categories)
{
	/*OJO!!!!!!!!!!En los helper se tiene que cargar una instancia del CI para pode usar sus funciones*/
	$CI =& get_instance();
	$data_rows='<ul class="dropdown-menu"><li>';
	$data_rows.='<div class="yamm-content"><div class="row">';
	foreach($categories->result() as $categorie)
	{
		$data_rows.='<div class="col-sm-3">';
		$data_rows.='<h5>'.$categorie->NombreCategoria.'</h5>';
		$data_rows.='<ul>';
		$subcategorias=$CI->Subcategoria->buscar_subcategoria($categorie->idCategoria);
		foreach($subcategorias->result() as $subcategoria)
		{
			$data_rows.="<li><a href='".base_url().'catalogo/'.$subcategoria->NombreSubCategoria."'>".$subcategoria->NombreSubCategoria."</a></li>";
		}
		//
		$data_rows.='</ul>';
		$data_rows.='</div>';
	}
	$data_rows.='</div>';
	$data_rows.='</li></ul>';
	return $data_rows;
}


?>