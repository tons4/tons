<?php
function ver_productos($productos, $paginaActual, $Subcategoria)
{
	$CI =& get_instance();
	$data ='';
    
    
    foreach ($productos->result() as $product) 
    {
    	$data .='   <div class="col-md-4 col-sm-6">
                    <div class="product">
                    <div class="flip-container">
                    <div class="flipper">
                    <div class="front">
                    <a href="">
                    <img src="'.base_url().$product->Portada.'" alt="" class="img-responsive">
                    </a>
                    </div>
                    <div class="back">
                    <a href="'.base_url().'detalles/'.$product->idProducto.'">
                    <img src="'.base_url().$product->Img.'" alt="" class="img-responsive">
                    </a>
                    </div>
                    </div>
                    </div>
                    <a href="" class="invisible">
                    <img src="'.base_url().$product->Img.'" alt="" class="img-responsive">
                    </a>
                    <div class="text">
                    <h3><a href="">'.$product->Nombre.'</a></h3>
                    <p class="price">$'.$product->Precio.'</p>
                    <p class="buttons">
                    <a href="'.base_url().'detalles/'.$product->idProducto.'" class="btn btn-default">Detalles</a>
                    <a onclick="Registrarcarrito('.$product->idProducto.')" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>Agregar</a>
                    </p>
                    </div>
                    <!-- /.text -->
                    </div>
                    <!-- /.product -->
                    </div>';
    }
    
    $data .='       </div>
                    <div class="pages">
                    <ul class="pagination">';

   $paginas =$CI->Producto->ver_cantidad_producto($Subcategoria)->row();
   if($paginas->Canti != 0)
   {
       $cantidad =(3%$paginas->Canti);

       for ($i=1; $i <=$cantidad ; $i++) 
        { 
            if($paginaActual == $i)
            {
            $data.='<li class="active"><a href="'.base_url().'catalogo/'.$Subcategoria.'/'.$i.'">'.$i.'</a></li>';
            }
            else
            {
            $data .='<li><a href="'.base_url().'catalogo/'.$Subcategoria.'/'.$i.'">'.$i.'</a></li>';
            }
        }

   }

   
   
                    
                    
    $data .='       </ul>
                    </div>
                    </div>
                    </div>
                    </div>
                    ';

                    
    return $data;
}

function buscar_producto($Nombre)
{
    
    $data='';
    
    foreach ($Nombre->result() as $productos)
     {
        $data .='
        <table class="table">
        <tbody>
        <tr>
        <td>
        <h3>
        <center>
        <a href="'.base_url().'detalles/'.$productos->idProducto.'">'.$productos->Nombre.'</a>
        </h3>
        </center>
        </td>
        </tr>
        </tbody>
        </table>';
    }
   

    return $data;
}

function comentarios_producto($idProducto)
{
    $CI =& get_instance();
    $data='';

    $comentarios = $CI->Comentario->buscar_comentario($idProducto);


    foreach ($comentarios->result() as $com)
    {
        $data .='<div class="row comment">
                <div class="col-sm-3 col-md-2 text-center-xs">
                <p>
                <img src="'.base_url().$com->imgP.'" class="img-responsive img-circle" alt="">
                </p>
                </div>
                <div class="col-sm-9 col-md-10">
                <h5>'.$com->Nombre.'</h5>
                <p class="posted"><i class="fa fa-clock-o"></i>'.$com->fecha.'</p>
                <p>'.$com->comentario.'</p>
                </div>
                </div>';
       
    } 

    return $data;        
}

?>