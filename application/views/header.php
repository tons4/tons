<!-- *** nav bar ***
_________________________________________________________ -->
<div class="navbar navbar-default yamm" role="navigation" id="navbar">
<div class="container">
<div class="navbar-header">
<a class="navbar-brand home" href="<?php echo base_url() ?>" data-animate-hover="bounce">
<img src="<?php echo base_url() ?>img/logo.png" alt="logo" class="hidden-xs">
<img src="<?php echo base_url() ?>img/logo_dev.png" alt="logo" class="visible-xs"><span class="sr-only">BE Healthy Shop - inicio</span>
</a>
<div class="navbar-buttons">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
<span class="sr-only">Menu</span>
<i class="fa fa-align-justify"></i>
</button>
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
<span class="sr-only">Buscar</span>
<i class="fa fa-search"></i>
</button>
<a href="#" data-toggle="modal" data-target="#login-modal" class="btn btn-default navbar-toggle"><i class="fa fa-sign-in"></i><span class="hidden-xs">Login</span></a>
</div>
</div>
<!--/.navbar-header -->
<div class="navbar-collapse collapse" id="navigation">
<ul class="nav navbar-nav navbar-left">
<li><a href="<?php echo base_url() ?>">Inicio</a>
</li>
<li class="dropdown yamm-fw">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Categorias<b class="caret"></b></a>
<?php echo $categorias_nav; ?>

</li>
</div>
<!--/.nav-collapse -->
<div class="navbar-buttons">
<div class="navbar-collapse collapse right">
<?php if($this->user_lib->get_user() == -1){ ?>

<a href="#" data-toggle="modal" data-target="#login-modal" class="btn btn-primary navbar-btn"><i class="fa fa-sign-in">
</i><span class="hidden-sm">Login</span></a>


<a href="/carrito" class="btn btn-primary navbar-btn"><i class="fa fa-shopping-cart"></i><span class="hidden-sm"></span></a>
<?php }else{ ?>
<div class="navbar-collapse collapse right" id="basket-overview">
<a href="/carrito" class="btn btn-primary navbar-btn"><i class="fa fa-shopping-cart"></i><span class="hidden-sm"></span></a></div>
<div class="navbar-collapse collapse right"><a href="/Usuarios/salir"  class="btn btn-primary navbar-btn"><i class="fa fa-user"></i><span class="hidden-sm"></span></a>
</div>
<?php  } ?>
</div>
<!--/.nav-collapse -->
<div class="navbar-collapse collapse right" id="search-not-mobile">
<button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search">
<span class="sr-only">Buscar</span>
<i class="fa fa-search"></i>
</button>
</div>
</div>
<div class="collapse clearfix" id="search">
<form class="navbar-form" role="search">
<div class="input-group">
<input type="text" class="form-control" placeholder="Buscar" id="meme">
<span class="input-group-btn">
<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
</span><br>
</div>
</form>
<diV id="dd" class="col-md-12">  </div>
</div>
<!--/.nav-collapse -->
</div>
<!-- /.container -->
</div>
<!-- /#navbar -->