<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="BE Healthy Shop- e-commerce">
<meta name="author" content="We are healthy we save de world">
<meta name="keywords" content="">
<title>
BE Healthy Shop
</title>
<meta name="keywords" content="">
<!-- styles -->


<link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/owl.theme.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/style.default.css" rel="stylesheet" id="theme-stylesheet">
<!-- your stylesheet with modifications -->
<link href="<?php echo base_url();?>css/custom.css" rel="stylesheet">


<link rel="shortcut icon" href="<?php echo base_url();?>img/logo/favicon.png">
</head>
<body>

<div id="Res"></div>

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h4 class="modal-title" id="Login">Login</h4>
</div>
<div class="modal-body">
<form action="customer-orders.html" method="post" id="Login2">
<div class="form-group">
<input type="text" class="form-control" id="usuario" placeholder="Usuario">
</div>
<div class="form-group">
<input type="password" class="form-control" id="contraseña" placeholder="Contraseña">
</div>
<p class="text-center">
<p><a  class="btn btn-success" onclick="log()" >Iniciar Session</a></p>
</p>
</form>
<p class="text-center text-muted">¿Aun no estas regitrado?</p>
<p class="text-center text-muted"><a href="#" data-toggle="modal" data-target="#registro-modal" data-dismiss="#login-modal"><strong>Registrate ahora</strong></a>! Es facil solo te toma 1 minuto hacerlo :D</p>
</div>
</div>
</div>
</div>

<div class="modal fade" id="registro-modal" tabindex="-1" role="dialog" aria-labelledby="Registro" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h4 class="modal-title" id="Registro">Registrar nuevo usuario</h4>
</div>
<div class="modal-body">
<form action="..<?php echo base_url();?>usuario/Registro" method="post" onsubmit="return validarInputs();">
<div class="form-group">
<input type="text" class="form-control" id="usuario1" placeholder="Usuario">
</div>
<div class="form-group">
<input type="password" class="form-control" id="pass1" placeholder="Contraseña">
</div>
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label for="Nombre">Nombre completo</label>
<input type="text" class="form-control" id="Nombre1">
</div>
</div>
<div class="col-sm-3">
<div class="form-group">
<label for="ApellidoP">Apellido Paterno</label>
<input type="text" class="form-control" id="apellidoP1">
</div>
</div>
<div class="col-sm-3">
<div class="form-group">
<label for="ApellidoM">Apellido Materno</label>
<input type="text" class="form-control" id="apellidoM1">
</div>
</div>
</div>
<!-- /.row -->
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label for="company">Numero de casa</label>
<input type="text" class="form-control" id="Direccion1">
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label for="street">Calle</label>
<input type="text" class="form-control" id="street1">
</div>
</div>
</div>
<!-- /.row -->
<div class="row">
<div class="col-sm-6 col-md-3">
<div class="form-group">
<label for="city">Ciudad</label>
<input type="text" class="form-control" id="ciudad1">
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="form-group">
<label for="zip">Codigo postal</label>
<input type="text" class="form-control" id="cp1">
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="form-group">
<label for="state">Estado</label>
<input type="text" class="form-control" id="estado1">
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="form-group">
<label for="country">Pais</label>
<input type="text"  class="form-control" id="pais1">
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label for="phone">Telefono</label>
<input type="text" class="form-control" id="telefono1">
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label for="email">Correo electronico</label>
<input type="text" class="form-control" id="correo1">
</div>
</div>
<div class="col-sm-12 text-center">
<a onclick="validarInputs()" class="btn btn-primary">Guardar</a>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<!-- *** final del modal*** -->
<?php $this->load->view('header'); ?>

<?php

if($vista=='producto')
{
    $this->load->view('productos', $data); 
}
elseif($vista=='404')
{
	$this->load->view('error_404');
}
elseif($vista=='carrito')
{   
    echo '<div id="all">';
	$this->load->view('carrito', $data);
	echo '</div>';
}
?>


<?php 
if(!empty($data['js']))
{
	$this->load->view('footer',$data);
}
else
{
   $this->load->view('footer');
}	
 ?>

<!--  fin container -->
</div>
</div>
</body>
</html>