<?php if($tipo=='catalogo'){ ?>
<div id="all"> 
<div id="content">
<div class="container">
<div class="col-md-12">
<ul class="breadcrumb">
<li><a href="#">Inicio</a></li>
<li><a href="#"><?php echo $datos_categoria->NombreCategoria ?></a></li>
<li><a href="#"><?php echo $datos_categoria->NombreSubCategoria?></a></li>
</ul>
</div>
<div class="col-md-12">
<div class="box">
<h1><?php echo $datos_categoria->NombreSubCategoria?></h1>
<p><?php echo $datos_categoria->Descripcion?></p>
</div><div class="row products ">
<!-- --------------------inicio de lista de productos-------------------------- -->	
<?php echo $productos; ?>


<?php } elseif($tipo=='detalles'){  ?>
<div id="content">
<div class="container">
<!-- ----------------------------imagen producto principal---------------- -->
<div class="col-md-12">
<div class="row" id="productMain">
<div class="col-sm-6">
<div id="mainImage">
<img src="<?php echo base_url().$producto->Portada; ?>" alt="" class="img-responsive">
</div>
</div>
<div class="col-sm-6">
<div class="box">
<h1 class="text-center"><?php echo $producto->Nombre; ?></h1>
<p class="goToDescription"><a href="#details" class="scroll-to">Desliza hacia abajo o clic aqui para mostrar los detalles.</a>
</p>
<p class="price">$<?php echo $producto->Precio; ?></p>
<p class="text-center buttons">
<a href="#" onclick="Registrarcarrito(<?php echo $producto->idProducto; ?>)" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Agregar</a> 
</p>
</div>
<!-- menu imagenes de producto para mostrar -->
<div class="row" id="thumbs">
<div class="col-xs-4">
<a href="<?php echo base_url().$producto->Portada; ?>" class="thumb">
<img src="<?php echo base_url().$producto->Portada; ?>" alt="" class="img-responsive">
</a>
</div>
<div class="col-xs-4">
<a href="<?php echo base_url().$producto->Img; ?>" class="thumb">
<img src="<?php echo base_url().$producto->Img; ?>" alt="" class="img-responsive">
</a>
</div>
<div class="col-xs-4">
<a href="<?php echo base_url().$producto->Img2; ?>" class="thumb">
<img src="<?php echo base_url().$producto->Img2; ?>" alt="" class="img-responsive">
</a>
</div>
</div>
<!-- fin menu de imagenes para mostrar -->
</div>
</div>
<!-- ------------------Seccion de detalles del producto_---------------------------- -->
<div class="box" id="details">
<p>
<blockquote>
<p><em><?php echo $producto->Descripcion; ?> Solo quedan : <?php echo $producto->Cantidad; ?></em>
</p>
</blockquote>
<!-- --------------termina seccion de detalles del producto------------- -->
<hr>
<br>
<!-- inicio de seccion de comentarios -->
<h1>Comentarios</h1>	
<?php echo $comentarios; ?>
<!-- seccion de formulario de comentarios -->
<div id="comment-form" >
<h4>Dejar un comentarios</h4>
<form>
<div class="row">
<div class="col-sm-12">
<div class="form-group">
<label for="comment">Comentario <span class="required">*</span>
</label>
<textarea class="form-control" id="comentario" rows="4"></textarea>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-12 text-right">
<a class="btn btn-primary" onclick="comenU(<?php echo $producto->idProducto; ?>)"><i class="fa fa-comment-o"></i> Comentar</a>
</div>
</div>
</form>
</div>
<!-- fin seccion formulario coemntarios -->
<!-- fin seccion de comentarios-->
</div>
</div>
</div>
<!-- /.container -->
</div>
<!-- /#content -->	

<?php }  ?>


