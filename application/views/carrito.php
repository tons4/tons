
<div id="content">
<div class="container">
<!-- tabal de productos -->
<div class="row">
<div class="col-md-12">
<ul class="breadcrumb">
<li><a href="#">Inicio</a>
</li>
<li>Carrito de compras</li>
</ul>
</div>
<div class="col-md-9" id="basket">
<div class="box">
<form method="post" action="checkout1.html">
<h1>Carrito de compras</h1>
<p class="text-muted">Usted tiene <?php echo $cantidad ?> productos en el carrito.</p>
<div class="table-responsive">
<table class="table">
<thead>
<tr>
<th colspan="2">Producto</th>
<th>Cantidad</th>
<th>Precio</th>
<th colspan="2">Total</th>
</tr>
</thead>
<!-- lista de productos -->
<tbody>
<?php echo $contenido ?>	
</tbody>
<tfoot>
<!-- termina lista de productos -->
<tr>
<th colspan="5">Total</th>
<th colspan="2">$<?php echo $total; ?></th>
</tr>
</tfoot>
</table>
</div>
<!-- /.table-responsive -->
<div class="box-footer">
<div class="pull-left">
<a href="'.Ruta.'" class="btn btn-default"><i class="fa fa-chevron-left"></i> Seguir comprando</a>
</div>
<div class="pull-right">
<a href="#" onclick="comprar(`'.$ref.'`)" class="btn btn-primary">Proceder con la compra <i class="fa fa-chevron-right"></i></a>
</div>
</div>
</form>
</div>
</div>
<!-- /.box -->
<div class="col-md-3">
<div class="box" id="order-summary">
<div class="box-header">
<h3>Total de la compra</h3>
</div>
<p class="text-muted">Las compras y el I.V.A son agregados por defecto</p>
<div class="table-responsive">
<table class="table">
<tbody>
<tr>
<td>Total de la compra</td>
<th>$<?php echo $total; ?></th>
</tr>
<tr class="total">
<td>Total</td>
<th>$<?php echo $total; ?></th>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript"><?php echo $js; ?></script>

  