<div id="footer" >
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-6">
<h4>Informacion</h4>
<ul>
<li><a href="/tien/text">Informacion y todos sobre nosotros.</a>
</li>
</ul>
<hr>
<hr class="hidden-md hidden-lg hidden-sm">
</div>
<!-- fin col-md-3 -->
<div class="col-md-3 col-sm-6">
<hr class="hidden-md hidden-lg">
</div>
<!-- fin .col-md-3 -->
<div class="col-md-3 col-sm-6">
<h4>Donde encontrarnos</h4>
<p><strong>e-commerce BE Healthy Shop.</strong>
<br>100 Ave. insurgentes
<br>col. la loma
<br>Tepic
<br>Nayarit
<br>
<strong>Mexico</strong>
</p>
<hr class="hidden-md hidden-lg">
</div>
<!-- fin col-md-3 -->
<h4>Siguenos en nuestras redes sociales</h4>
<p class="social">
<a href="#" class="facebook external" ><i class="fa fa-facebook"></i></a>
<a href="#" class="twitter external" ><i class="fa fa-twitter"></i></a>
<a href="#" class="instagram external" ><i class="fa fa-instagram"></i></a>
<a href="#" class="gplus external" ><i class="fa fa-google-plus"></i></a>
<a href="#" class="email external" ><i class="fa fa-envelope"></i></a>
</p>
</div>
<!-- fin col-md-3 -->
</div>
<!-- fin row -->
</div>
<script src="<?php echo base_url();?>js/jquery-1.11.0.min.js"></script>
<script type="text/javascript"><?php if(!isset($js)? '' : " "){echo $js;} ?></script>
<script src="<?php echo base_url();?>js/respond.min.js"></script>
<script src="<?php echo base_url();?>js/main.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.cookie.js"></script>
<script src="<?php echo base_url(); ?>js/waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap-hover-dropdown.js"></script>
<script src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>js/front.js"></script>
<script src="<?php echo base_url(); ?>js/Form.js"></script>
<script src="<?php echo base_url(); ?>js/sweetalert.min.js"></script>

