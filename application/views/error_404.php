<div id="all">
<div id="content">
<div class="container">
<div class="col-md-12">
<ul class="breadcrumb">
<li><a href="#">Home</a>
</li>
<li>Página no encontrada</li>
</ul>
<div class="row" id="error-page">
<div class="col-sm-6 col-sm-offset-3">
<div class="box">
<p class="text-center">
<img src="<?php echo base_url(); ?>img/logo.png" >
</p>
<h3>Lo sentimos</h3>
<h4 class="text-muted">Error 404 - Página no encontrada</h4>
<p class="buttons"><a href="<?php echo base_url(); ?>" class="btn btn-primary"><i class="fa fa-home"></i> Ir a la página de inicio</a>
</p>
</div>
</div>
</div>
</div>
<!-- /.col-md-9 -->
</div>
<!-- /.container -->
</div>
<!-- /#content -->

