<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="BE Healthy Shop- e-commerce">
<meta name="author" content="We are healthy we save de world">
<meta name="keywords" content="">
<title>
BE Healthy Shop
</title>
<meta name="keywords" content="">
<!-- styles -->


<link href="<?php echo base_url();?>css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/owl.theme.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/style.default.css" rel="stylesheet" id="theme-stylesheet">
<!-- your stylesheet with modifications -->
<link href="<?php echo base_url();?>css/custom.css" rel="stylesheet">


<link rel="shortcut icon" href="<?php echo base_url();?>img/logo/favicon.png">
</head>
<body>

<div id="Res"></div>

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h4 class="modal-title" id="Login">Login</h4>
</div>
<div class="modal-body">
<form action="Front/login" method="post" id="Login2">
<div class="form-group">
<input type="text" class="form-control" id="usuario" placeholder="Usuario">
</div>
<div class="form-group">
<input type="password" class="form-control" id="contraseña"  placeholder="Contraseña">
</div>
<p class="text-center">
<p><a  class="btn btn-success" onclick="log()" >Iniciar Session</a></p>
</p>
</form>
<p class="text-center text-muted">¿Aun no estas regitrado?</p>
<p class="text-center text-muted"><a href="#" data-toggle="modal" data-target="#registro-modal" data-dismiss="#login-modal"><strong>Registrate ahora</strong></a>! Es facil solo te toma 1 minuto hacerlo :D</p>
</div>
</div>
</div>
</div>

<div class="modal fade" id="registro-modal" tabindex="-1" role="dialog" aria-labelledby="Registro" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h4 class="modal-title" id="Registro">Registrar nuevo usuario</h4>
</div>
<div class="modal-body">
<form action="..<?php echo base_url();?>usuario/Registro" method="post" onsubmit="return validarInputs();">
<div class="form-group">
<input type="text" class="form-control" id="usuario1" placeholder="Usuario">
</div>
<div class="form-group">
<input type="password" class="form-control" id="pass1" placeholder="Contraseña">
</div>
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label for="Nombre">Nombre completo</label>
<input type="text" class="form-control" id="Nombre1">
</div>
</div>
<div class="col-sm-3">
<div class="form-group">
<label for="ApellidoP">Apellido Paterno</label>
<input type="text" class="form-control" id="apellidoP1">
</div>
</div>
<div class="col-sm-3">
<div class="form-group">
<label for="ApellidoM">Apellido Materno</label>
<input type="text" class="form-control" id="apellidoM1">
</div>
</div>
</div>
<!-- /.row -->
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label for="company">Numero de casa</label>
<input type="text" class="form-control" id="Direccion1">
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label for="street">Calle</label>
<input type="text" class="form-control" id="street1">
</div>
</div>
</div>
<!-- /.row -->
<div class="row">
<div class="col-sm-6 col-md-3">
<div class="form-group">
<label for="city">Ciudad</label>
<input type="text" class="form-control" id="ciudad1">
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="form-group">
<label for="zip">Codigo postal</label>
<input type="text" class="form-control" id="cp1">
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="form-group">
<label for="state">Estado</label>
<input type="text" class="form-control" id="estado1">
</div>
</div>
<div class="col-sm-6 col-md-3">
<div class="form-group">
<label for="country">Pais</label>
<input type="text"  class="form-control" id="pais1">
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label for="phone">Telefono</label>
<input type="text" class="form-control" id="telefono1">
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label for="email">Correo electronico</label>
<input type="text" class="form-control" id="correo1">
</div>
</div>
<div class="col-sm-12 text-center">
<a onclick="validarInputs()" class="btn btn-primary">Guardar</a>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<!-- *** final del modal*** -->
<?php $this->load->view('header'); ?>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
<!-- Indicators -->
<ol class="carousel-indicators">
<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
<li data-target="#myCarousel" data-slide-to="1"></li>
<li data-target="#myCarousel" data-slide-to="2"></li>
</ol>
<!-- Wrapper for slides -->
<div class="carousel-inner">
<div class="item active">
<img src="<?php echo base_url();?>img/Slider-img/ecologi.jpg" alt="img-1" style="width: 100%; height: 100%;" >
</div>
<div class="item">
<img src="<?php echo base_url();?>img/Slider-img/pro-1.jpg" alt="img-3" style="width: 100%; height: 100%;">
</div>
<div class="item">
<img src="<?php echo base_url();?>img/Slider-img/producto_123.png" alt="img-3" style="width: 100%; height: 100%;">
</div>
</div>
<!-- Left and right controls -->
<a class="left carousel-control" href="#myCarousel" data-slide="prev">
<span class="fa fa-arrow "></span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#myCarousel" data-slide="next">
<span class="glyphicon glyphicon-chevron-right"></span>
<span class="sr-only">Next</span>
</a>
</div>  
<br>
<div id="all">
<div id="content">
<div class="container">
<div class="col-md-12">
</div>
</div>
<div id="advantages">
<div class="container">
<div class="same-height-row">
<div class="col-sm-4">
<div class="box same-height clickable">
<div class="icon"><i class="fa fa-heart"></i>
</div>
<h3><a href="#">Amamos a nuestro planeta</a></h3>
<p>Nuestro planeta debe de ser resguardado por nosotros, por eso contribuye comprando productos elaborados con materia prima 100% de materiales reciclados</p>
</div>
</div>
<div class="col-sm-4">
<div class="box same-height clickable">
<div class="icon"><i class="fa fa-tags"></i>
</div>
<h3><a href="#">Mejores precios</a></h3>
<p>Conprometidos con tu bolsillo asi que nuestros precios son calculados para poder ofrecer un mejor ahorro, ademas de que algunos productos incluyen un pequeño pero importante descuento</p>
</div>
</div>
<div class="col-sm-4">
<div class="box same-height clickable">
<div class="icon"><i class="fa fa-thumbs-up"></i>
</div>
<h3><a>Producto 100% de calidad</a></h3>
<p>Nuestros productos estas elaborados con altos estandares de produccion
<br>
y bajo normativas que garantizan productos de alta calisda como si se 
<br>
tratase de materia prima nueva.
</p>
</div>
</div>
</div>
<!-- row -->
</div>
<!--container -->
</div>
<div class="container" >
<div class="col-md-12">
<div class="box slideshow">
<h3>Porque debemos reciclar?</h3>
<p class="lead">De cada 100 Kg. de basura sólo el 70% se recolecta, más de 30 mil toneladas diarias llegan a barrancos, ríos y terrenos baldíos convirtiéndose en agentes contaminantes y fuentes de infección. La mayor parte de los desechos son reutilizables y reciclables, el problema está en que al mezclarlos se convierten en basura.
En promedio una familia genera mensualmente basura constituida por papel, cartón, vidrio, metal, plásticos y desechos de control sanitario. Si se aprende a separarla adecuadamente podremos controlarlos y evitar posteriores problemas.
Al separar nuestros desperdicios correctamente antes de que se conviertan en basura es posible reducir un 80% del espacio total que ésta ocupa.
Para poder iniciar con este proceso es importante entender qué es “reciclar”. Reciclar es el proceso mediante el cual los productos de desecho son nuevamente utilizados y tiene por objeto la recuperación, de forma directa o indirecta, de los componentes que contienen los residuos urbanos.
La importancia de hacerlo es que nos puede ayudar a resolver muchos de los problemas creados por la forma de vida moderna. Se pueden salvar grandes cantidades de recursos naturales no renovables cuando en los procesos de producción se utilizan materiales reciclados. Los recursos renovables, como los árboles, también pueden ser salvados. La utilización de productos reciclados disminuye el consumo de energía. Cuando se consuman menos combustibles fósiles, se generará menos CO2 y por lo tanto habrá menos lluvia ácida y se reducirá el efecto invernadero.</p>
<div id="get-inspired" class="owl-carousel owl-theme">
<div class="item">
<a href="#">
<img src="<?php echo base_url();?>img/Slider-img/conta/arboles.png" alt="Get inspired" class="img-responsive">
</a>
</div>
<div class="item">
<a href="#">
<img src="<?php echo base_url();?>img/Slider-img/conta/conta_aire.jpg" alt="Get inspired" class="img-responsive">
</a>
</div>
<div class="item">
<a href="#">
<img src="<?php echo base_url();?>img/Slider-img/conta/conta_ba.jpg" alt="Get inspired" class="img-responsive">
</a>
</div>
</div>
</div>
</div>
</div>


<?php $this->load->view('footer'); ?>
<!--  fin container -->
</div>
</div>
</body>
</html>